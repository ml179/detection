import datetime
import numpy as np
import cv2

def stime(dt):
    return dt.strftime("%Y-%m-%d-%H-%M-%S")


def parse_time(stime):
    return datetime.datetime.strptime(stime, "%Y-%m-%d-%H-%M-%S")


def crop_image(img, x, y, w, h):
    return img[y:y+h, x:w]


def convert_binary(image, thresh_val):
    # преобразуем изображение из оттенков серого в бинарную маску
    if image.dtype == np.float32:
        image = (255 * (image - np.min(image)) / np.ptp(image)).astype(np.uint8)

    initial_conv = np.where((image<= thresh_val), image, 255)
    return np.where((initial_conv > thresh_val), initial_conv, 0)


def normalize(image):
    # нормализация, которая работает на отрицательных значениях
    return (255*(image - np.min(image))/np.ptp(image)).astype(np.uint8)


def mean_std_metrics(video, interval=0.01):
    # вычисляем примерное среднее и стандартное отклонение,
    # не обходя всё видео целиком

    if type(interval) == float:
        interval = len(video) * interval

    mean, std = 0, 0
    count = int(len(video) / interval) - 1

    for i in range(0, count):
        im = video[int(i * interval)]
        mean += im.mean()
        std += im.std()

    mean /= count
    std /= count
    return mean, std


def mean_image(video, interval=0.01):
    if type(interval) == float:
        interval = len(video) * interval

    res = np.zeros_like(video[0], dtype=np.uint64)

    count = int(len(video) / interval) - 1
    for i in range(0, count):
        try:
            res = res + video[int(i * interval)]
        except:
            count -= 1

    return (res / count).astype(np.uint8)
    

def apply_mask(im, mask):
    if im is None:
        return None

    return cv2.bitwise_and(im, im, mask = mask)


def show_img(im, wait=True):
    cv2.imshow('ims', (im))
    if wait:
        while True:
            key = cv2.waitKey(1) & 0xff
            if key==ord('w'):
                break
            elif key==ord('q'):
                exit()
    else:
        key = cv2.waitKey(50)
        if key == ord('q'):
            exit()
