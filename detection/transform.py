import detection.filter as filter
import cv2
import detection.preprocess as preprocess
import detection.utils as utils
import numpy as np


def image_dif(im, mean_image):
    dif = mean_image.astype(np.int16) - im.astype(np.int16)
    dif[dif < 150] = 0
    dif = dif.astype(np.uint8)
    return dif


def xray(image):
    image = preprocess.apply_brightness_contrast(image, 64, 64)
    image = preprocess.apply_brightness_contrast(image, 64, 64)

    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    image = filter.high_pass_filter(image, 10, 1)
    image = filter.low_pass_filter(image, 50)

    image = (image - np.min(image))/np.ptp(image)
    image = (image * 255).astype(np.uint8)
    image = preprocess.apply_brightness_contrast(image, 75, 64)

    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                cv2.THRESH_BINARY, 201, -30)
    return image


def erode_skelets(image):
    size = np.size(image)
    skel = np.zeros(image.shape,np.uint8)

    _, img = cv2.threshold(image,127,255,0)
    element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
    done = False
    
    while (not done):
        eroded = cv2.erode(img,element)
        temp = cv2.dilate(eroded,element)
        temp = cv2.subtract(img,temp)
        skel = cv2.bitwise_or(skel,temp)
        img = eroded.copy()
    
        zeros = size - cv2.countNonZero(img)
        if zeros==size:
            done = True
    return skel


def fill_gaps(img):
    erode_kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2))
    diliate_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    for _ in range(15):
        img = cv2.erode(img, erode_kernel, iterations=1)
        img = cv2.dilate(img, diliate_kernel, iterations=1)

    img = cv2.erode(img, erode_kernel, iterations=1)

    return img


def filter_contours(img, bin_mask, min_area=3000):
    if bin_mask is None:
        return None

    img = img.copy()
    contours, _ = cv2.findContours(bin_mask, 
        cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(contours, key=cv2.contourArea)

    mask = np.zeros_like(bin_mask, dtype=np.uint8)
    for cnt in contours:
        if (cv2.contourArea(cnt) > min_area):   
            # cv2.drawContours(img, [cnt], -1, (0, 255, 0), 2)
            cv2.drawContours(mask, [cnt], -1, 255, cv2.FILLED)

    return mask


def optical_flow(ims):
    prev = ims[0]
    next = ims[1]

    flow = cv2.calcOpticalFlowFarneback(prev, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
    
    flow = np.zeros((prev.shape[0], prev.shape[1], 2), dtype=np.uint8)
    flow[..., 0] = ang * 180 / np.pi / 2
    flow[..., 1] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)

    return flow


def color_flow(im):
    if im is None:
        return None

    hsv = np.zeros((im.shape[0], im.shape[1], 3), dtype=np.uint8)
    hsv[..., 0] = im[..., 0]
    hsv[..., 1] = 255
    hsv[..., 2] = im[..., 1]

    return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)


def heatmap_filter(thres_video, max=200):
    man_image = utils.mean_image(thres_video)
    man_image[man_image==255] = 255
    man_image[(man_image>0) & (man_image<max)] = 0
    return utils.apply_mask(thres_video[0], man_image)


def draw_blend(im, flow):
    if flow is None:
        return None
    return cv2.addWeighted(im, 0.5, flow, 0.5, 0.0)


def draw_contours(im, mask, color=(0, 0, 255), l=2):
    if mask is None:
        return None

    im = im.copy()
    contours, _ = cv2.findContours(mask, 
        cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(im, contours, -1, color, l)
    return im

