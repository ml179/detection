import numpy as np
import cv2
import detection.utils as utils


def prepare_frame(frame, size=(500, 500), crop=(220, 820)):
    #frame = cv2.resize(frame, (960, 540))
    #frame = utils.crop_image(frame, crop[0], 0, crop[1], frame.shape[1])
    #frame = cv2.resize(frame, size)
    return frame 


def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow
        
        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()
    
    if contrast != 0:
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)
        
        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf
