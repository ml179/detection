import psycopg2
import detection.load as load
import detection.utils as utils
import os
import re
import datetime
from contextlib import closing

DBNAME = 'pgdockerdatabase'
USER = 'pgdockeruser'
PASSWORD='pgdockerpass'
HOST = 'localhost'
DIR = '/mnt/c/Users/binarycat/YandexDisk/FishMl'

def connect(f):
    with closing(psycopg2.connect(
        dbname=DBNAME, user=USER, password=PASSWORD, host=HOST, port=5400)) as conn:
        with conn.cursor() as cursor:
            res = f(cursor)
            conn.commit()
    return res


def get_class_videos(video_pathes):
    r = re.compile('(\w+)_(\d{4})-(\d{2})-(\d{2})-(\d{2})-(\d{2})-(\d{2}).mp4')

    res = []
    for vid in video_pathes:
        base = os.path.basename(vid)
        m = r.match(base)
        if m is not None:
            cam = m.group(1)
            dtime = datetime.datetime(
                int(m.group(2)), int(m.group(3)),
                int(m.group(4)), int(m.group(5)),
                int(m.group(6)), int(m.group(7)))
            res.append((cam, dtime, dtime + + datetime.timedelta(minutes=1)))

    return res


def get_labels(cursor):
    d = {}
    cursor.execute('SELECT lbl_id, lbl_name FROM labels')
    for lbl in cursor.fetchall():
        id = lbl[0]
        name = lbl[1]
        d[name] = id
    return d


def insert_videos(cursor):
    labels = get_labels(cursor)
    dirs = load.get_files_per_dirs(DIR, labels.keys())
    for lbl in labels.keys(): 
        class_vids = get_class_videos(dirs[lbl])
        for _, beg, end in class_vids:
            cursor.execute(
                'INSERT INTO videos (cam_id, vid_beg, vid_end) VALUES (1, %s, %s)',
                (beg, end))
 

def assign_labels(cursor):
    labels = get_labels(cursor)
    dirs = load.get_files_per_dirs(DIR, labels.keys())

    for lbl in labels.keys(): 
        class_vids = get_class_videos(dirs[lbl])
        for _, beg, _ in class_vids:
            cursor.execute(
                'SELECT (vid_id) FROM videos WHERE vid_beg=%s',
                (beg, ))
            vid_id = cursor.fetchall()[0][0]
            lbl_id = labels[lbl]

            cursor.execute(
                'INSERT INTO video_labels (vid_id, lbl_id) VALUES (%s, %s)',
                (vid_id, lbl_id)
            )

def db_get_video_pathes(cursor):
    d = {}

    labels = get_labels(cursor)
    for lbl in labels.keys(): 
        lbl_id = labels[lbl]
        cursor.execute(
            'SELECT vid_beg FROM video_labels AS lbl JOIN videos AS vid ON vid.vid_id=lbl.vid_id WHERE lbl.lbl_id=%s',
            (lbl_id, )
        )

        res = []
        for t in cursor.fetchall():
            beg = t[0]
            path = f'{DIR}/{lbl}/IPCAM_{utils.stime(beg)}.mp4'
            res.append(path)
        
        d[lbl] = res
    
    return d


def db_get_the_latest_video(cursor):
    
    cursor.execute('SELECT vid_id, vid_beg FROM videos ORDER BY vid_id DESC LIMIT 1')
    for vids in cursor.fetchall():
        # vid_id = vids[0]
        vid_data = vids[1]
        path = f'{DIR}/IPCAM_{utils.stime(vid_data)}.mp4'
        return path
    return     


def get_video_pathes():
    return connect(db_get_video_pathes)

def get_the_latest_video():
    return connect(db_get_the_latest_video)