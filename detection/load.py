import cv2
import os
import numpy as np
import collections
import glob


def read_video(vid):
    vidcap = cv2.VideoCapture(vid)
    success,image = vidcap.read()
    res = []
    while success:
        image = cv2.resize(image, (50, 50))
        res.append(image.ravel())
        success, image = vidcap.read()

    return res


def read_video_from_dir(dir):
    data = []

    for v in os.listdir(dir):
        data += read_video(dir + '/' + v)
    data = np.matrix(data)

    return data


def get_files_per_dirs(root, dirs):
    files_for_class = collections.defaultdict(list)

    for class_dir in dirs:
        for f in os.listdir(root + '/' + class_dir):
            full = root + '/' + class_dir + '/' + f

            # чтобы случайно не передали локальную директорию
            path = os.path.normpath(full)
            tokens = path.split(os.sep)

            if len(tokens) >= 2:
                files_for_class[class_dir].append(full)

    return files_for_class


class VideoLoader:
    def __init__(self, video_path):
        self._path = video_path
        self._cap = cv2.VideoCapture(video_path)
        self._total_frames = int(self._cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self._fps = int(self._cap.get(cv2.CAP_PROP_FPS))

        if self._fps == 0:
            raise Exception('kek')

        self._time_length = self._total_frames / self._fps

    def __getitem__(self, i):
        if i >= 0 and i <= self._total_frames:
            self._cap.set(cv2.CAP_PROP_POS_MSEC , (i * 1000 / self._fps))
            ret, frame = self._cap.read()
            if ret:
                return frame
            else:
                return None

        return None
    
    def get_by_seconds(self, seconds):
        self._cap.set(cv2.CAP_PROP_POS_MSEC , seconds * 1000)
        ret, frame = self._cap.read()
        if ret:
            return frame
        else:
            return None
    
    def get_local_index(self, i):
        return i
    
    def get_range(self, ifrom, ito):
        return self.get_indices(range(ifrom, ito))
    
    def get_indices(self, indices):
        res = []
        for i in indices:
            res.append(self[i])
        return res
    
    def get_num_indices(self):
        return 1
    
    def get_parent_index(self, i):
        return 0
    
    def __len__(self):
        return self._total_frames
    
    def moment(self, i):
        return self._path, i / self._total_frames 

    def release(self):
        self._cap.release()

class ResourceDirLoader:
    def __init__(self, dir, fread):
        self.files_list = sorted(glob.glob(os.path.join(dir, '*')))
        self.fread = fread
    
    def __getitem__(self, i):
        if i >= 0 and i < len(self.files_list):
            return self.fread(self.files_list[i])
        else:
            print('---------------------------------------resdir')
            return None

    def get_range(self, ifrom, ito):
        return self.get_indices(range(ifrom, ito))
    
    def get_indices(self, indices):
        res = []
        for i in indices:
            res.append(self[i])
        return res
    
    def get_parent_index(self, i):
        return 0
    
    def get_num_indices(self):
        return 1

    def __len__(self):
        return len(self.files_list)
    
    def moment(self, i):
        return self.files_list[i], i / len(self.files_list) 
    
    def get_local_index(self, i):
        return i

    def release(self):
        pass

class DirLoader:
    def __init__(self, video_dir, Loader = VideoLoader):
        self._loader_infos = []
        self._total_frames = 0
        self.Loader = Loader

        for v in os.listdir(video_dir):
            video_path = video_dir +  '/' + v

            try:
                loader = self.Loader(video_path)
                l = len(loader)
                ni = loader.get_num_indices()
                self._total_frames += l
                self._loader_infos.append((video_path, l, ni))
                loader.release()
            except:
                print('не удалось открыть:', video_path)
    
    def __getitem__(self, i):
        cnt = 0
        for p, l, _ in self._loader_infos:
            if i < cnt + l:
                loader = self.Loader(p)
                frame = loader[i-cnt]
                loader.release()
                return frame
            cnt += l
        return None
    
    def get_local_index(self, i):
        cnt = 0
        for p, l, _ in self._loader_infos:
            if i < cnt + l:
                loader = self.Loader(p)
                idx = loader.get_local_index(i-cnt)
                loader.release()
                return idx
            cnt += l
        return None
    
    def get_parent_index(self, i):
        cnt = 0
        indices_cnt = 0
        for p, l, ni in self._loader_infos:
            if i < cnt + l:
                loader = self.Loader(p)
                idx = indices_cnt + loader.get_parent_index(i-cnt)
                loader.release()
                return idx
            cnt += l
            indices_cnt += ni
        return None
    
    def get_parent_index(self, i):
        cnt = 0
        for _, l, _ in self._loader_infos:
            if i < cnt + l:
                return cnt
            cnt += l

        return None
    
    def get_num_indices(self):
        return len(self._loader_infos)
    
    def get_range(self, ifrom, ito):
        result = []
        cnt = 0
        for p, l, _ in self._loader_infos:
            if ifrom < cnt + l:
                loader = self.Loader(p)
                m = min(cnt + l, ito)
                result.extend(loader.get_range(ifrom, m))
                ifrom = m
                loader.release()
            cnt += l

            if ifrom == ito:
                return result
        return None
    
    def get_indices(self, indices):
        result = []
        cnt = 0
        for p, l, _ in self._loader_infos:
            if indices[0] < cnt + l:
                loader = self.Loader(p)
                
                get_indices = []
                for idx in indices:
                    if idx < cnt + l:
                        get_indices.append(idx)
                        indices = indices[1:]
                    else:
                        break

                result.extend(loader.get_indices(get_indices))
                loader.release()
            cnt += l

            if len(indices) == 0:
                return result

        return None

    def __len__(self):
        return self._total_frames

    def moment(self, i):
        cnt = 0
        for p, l, _ in self._loader_infos:
            if i < cnt + l:
                loader = self.Loader(p)
                moment = loader.moment(i-cnt)
                loader.release()
                return moment
            cnt += l
        return None

    def release(self):
        pass


class VideoProccessor:
    def __init__(self, video, func, history=0, interval=5):
        if type(video) != list:
            self._videos = [video]
        else:
            self._videos = video
        self._func = func
        self._history = history
        self._interval = interval
    
    def __getitem__(self, i):
        frames = []
        if self._history == 0:
            frames = [vid[i] for vid in self._videos]
        else:
            if i >= (self._history * self._interval - 1):
                for vid in self._videos:
                    frames.append([])
                    for j in range(self._history):
                        frames[-1].append(vid[i - j * self._interval])
                    frames[-1].reverse()

        if len(frames) > 0:
            return self._func(*frames)

        return None
    
    def get_by_seconds(self, seconds):
        return self._videos[0].get_by_seconds(seconds)

    def moment(self, i):
        return self._videos[0].moment(i)
    
    def __len__(self):
        return len(self._videos[0])

    def get_range(self, ifrom, ito):
        return self.get_indices(range(ifrom, ito))
    
    def get_indices(self, indices):
        res = []
        for i in indices:
            res.append(self[i])
        return res
    
    def get_parent_index(self, i):
        return self._videos[0].get_parent_index(i)
    
    def get_num_indices(self):
        return self._videos[0].get_num_indices()
    
    def get_local_index(self, i):
        return self._videos[0].get_local_index(i)
    
    def release(self):
        for v in self._videos:
            v.release()
