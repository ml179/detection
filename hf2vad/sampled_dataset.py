import context as _
import numpy as np
from torch.utils.data import Dataset
import joblib
import torchvision.transforms as transforms

transform = transforms.Compose([
    transforms.ToTensor(),
])


class ChunkeSampleDataset(Dataset):
    def __init__(self, chunk_file, last_flow=False):
        super(ChunkeSampleDataset, self).__init__()
        self.chunk_file = chunk_file
        self.last_flow = last_flow

        # dict(sample_id=[], appearance=[], motion=[], bbox=[], pred_frame=[])
        self.chunked_samples = joblib.load(self.chunk_file)

        self.chunked_samples_appearance = self.chunked_samples["appearance"]
        self.chunked_samples_motion = self.chunked_samples["motion"]
        self.chunked_samples_bbox = self.chunked_samples["bbox"]
        self.chunked_samples_pred_frame = self.chunked_samples["pred_frame"]
        self.chunked_samples_id = self.chunked_samples["sample_id"]

    def __len__(self):
        return len(self.chunked_samples_id)

    def __getitem__(self, indice):
        appearance = self.chunked_samples_appearance[indice]
        motion = self.chunked_samples_motion[indice]
        bbox = self.chunked_samples_bbox[indice]
        pred_frame = self.chunked_samples_pred_frame[indice]

        # [#frame,h,w,c] to [h,w,#frame,c]
        x = np.transpose(appearance, [1, 2, 0, 3])
        x = np.reshape(x, (x.shape[0], x.shape[1], -1))

        y = motion[1:] if not self.last_flow else motion[-1:]
        y = np.transpose(y, [1, 2, 0, 3])
        y = np.reshape(y, (y.shape[0], y.shape[1], -1))

        x = transform(x)
        y = transform(y)

        return x, y, bbox.astype(np.float32), pred_frame, indice
