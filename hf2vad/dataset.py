import context as _
import torch
import numpy as np
import cv2
from collections import OrderedDict
import math
from torch.utils.data import IterableDataset
import detection.load as load


class bbox_collate:
    def __init__(self, mode):
        self.mode = mode

    def collate(self, batch):
        if self.mode == 'train':
            return bbox_collate_train(batch)
        elif self.mode == 'test':
            return bbox_collate_test(batch)
        else:
            raise NotImplementedError


def bbox_collate_train(batch):
    batch_data = [x[0] for x in batch]
    batch_target = [x[1] for x in batch]
    return torch.cat(batch_data, dim=0), batch_target


def bbox_collate_test(batch):
    batch_data = [x[0] for x in batch]
    batch_target = [x[1] for x in batch]
    return batch_data, batch_target


def get_foreground(img, bboxes, patch_size):
    """
    Cropping the object area according to the bouding box, and resize to patch_size
    :param img: [#frame,c,h,w]
    :param bboxes: [#,4]
    :param patch_size: 32
    :return:
    """
    img_patches = list()
    if len(img.shape) == 3:
        for i in range(len(bboxes)):
            x_min, x_max = np.int32(np.ceil(bboxes[i][0])), np.int32(np.ceil(bboxes[i][2]))
            y_min, y_max = np.int32(np.ceil(bboxes[i][1])), np.int32(np.ceil(bboxes[i][3]))
            cur_patch = img[:, y_min:y_max, x_min:x_max]
            if np.all(np.array(cur_patch.shape) > 0):
                cur_patch = cv2.resize(np.transpose(cur_patch, [1, 2, 0]), (patch_size, patch_size))
                img_patches.append(np.transpose(cur_patch, [2, 0, 1]))
            else:
                img_patches.append(np.zeros((img.shape[0], patch_size, patch_size), dtype=img.dtype))
        img_patches = np.array(img_patches)
    elif len(img.shape) == 4:
        for i in range(len(bboxes)):
            x_min, x_max = np.int32(np.ceil(bboxes[i][0])), np.int32(np.ceil(bboxes[i][2]))
            y_min, y_max = np.int32(np.ceil(bboxes[i][1])), np.int32(np.ceil(bboxes[i][3]))
            cur_patch_set = img[:, :, y_min:y_max, x_min:x_max]
            if np.all(np.array(cur_patch_set.shape) > 0):
                tmp_set = list()
                for j in range(img.shape[0]):  # temporal patches
                    cur_patch = cur_patch_set[j]
                    cur_patch = cv2.resize(np.transpose(cur_patch, [1, 2, 0]),
                                        (patch_size, patch_size))
                    tmp_set.append(np.transpose(cur_patch, [2, 0, 1]))
                cur_cube = np.array(tmp_set)  # spatial-temporal cube for each bbox
                img_patches.append(cur_cube)  # all spatial-temporal cubes in a single frame
            else:
                img_patches.append(np.zeros((img.shape[0], img.shape[1], patch_size, patch_size), dtype=img.dtype))
        img_patches = np.array(img_patches)
    
    return img_patches  # [num_bboxes,frames_num,C,patch_size, patch_size]


class common_dataset(IterableDataset):
    def __len__(self):
        raise NotImplementedError

    def __getitem__(self, indice):
        raise NotImplementedError

    def _context_range(self, indice):
        """
        get a clip according to the indice (i.e., the frame to be predicted)
        :param indice: be consistent with __getitem__()
        :return: the frame indices in the clip
        """
        if self.border_mode == "predict":
            if indice - self.context_frame_num < 0:
                start_idx = 0
            else:
                start_idx = indice - self.context_frame_num
            end_idx = indice
            need_ctx_frames = self.context_frame_num + 1  # future frame prediction
        else:
            if indice - self.context_frame_num < 0:
                start_idx = 0
            else:
                start_idx = indice - self.context_frame_num

            if indice + self.context_frame_num > self.tot_frame_num - 1:
                end_idx = self.tot_frame_num - 1
            else:
                end_idx = indice + self.context_frame_num
            need_ctx_frames = 2 * self.context_frame_num + 1

        center_frame_video_idx = self.get_frame_video_idx(indice)
        clip_frames_video_idx = self.get_frame_video_idx_range(start_idx, end_idx + 1)
        need_pad = need_ctx_frames - len(clip_frames_video_idx)

        if need_pad > 0:
            if start_idx == 0:
                clip_frames_video_idx = [clip_frames_video_idx[0]] * need_pad + clip_frames_video_idx
            else:
                clip_frames_video_idx = clip_frames_video_idx + [clip_frames_video_idx[-1]] * need_pad

        tmp = np.array(clip_frames_video_idx) - center_frame_video_idx
        offset = np.sum(tmp)

        if tmp[0] != 0 and tmp[-1] != 0:  # extreme condition that is not likely to happen
            print('The video is too short or the context frame number is too large!')
            raise NotImplementedError

        if need_pad == 0 and offset == 0:
            idx = [x for x in range(start_idx, end_idx + 1)]
            return idx
        else:
            if self.border_mode == 'predict':
                if need_pad > 0 and np.abs(offset) > 0:
                    print('The video is too short or the context frame number is too large!')
                    raise NotImplementedError
                idx = [x for x in range(start_idx - offset, end_idx + 1)]
                idx = [idx[0]] * np.maximum(np.abs(offset), need_pad) + idx
                return idx
            else:
                if need_pad > 0 and np.abs(offset) > 0:
                    print('The video is too short or the context frame number is too large!')
                    raise NotImplementedError
                if offset > 0:
                    idx = [x for x in range(start_idx, end_idx - offset + 1)]
                    idx = idx + [idx[-1]] * np.abs(offset)
                    return idx
                elif offset < 0:
                    idx = [x for x in range(start_idx - offset, end_idx + 1)]
                    idx = [idx[0]] * np.abs(offset) + idx
                    return idx
                if need_pad > 0:
                    if start_idx == 0:
                        idx = [x for x in range(start_idx, end_idx + 1)]
                        idx = [idx[0]] * need_pad + idx
                        return idx
                    else:
                        idx = [x for x in range(start_idx, end_idx + 1)]
                        idx = idx + [idx[-1]] * need_pad
                        return idx


class VideoDataset(common_dataset):
    def __init__(self, dir, context_frame_num=0, border_mode="hard",
                 file_format='.mp4', all_bboxes=None, patch_size=32, combine_datasets=None):
        super(VideoDataset, self).__init__()
        self.dir = dir
        self.video_dirs = OrderedDict()
        self.border_mode = border_mode
        self.file_format = file_format
        self.all_bboxes = all_bboxes
        self.patch_size = patch_size
        self.context_frame_num = context_frame_num
        self.combine_datasets = combine_datasets

        self._dataset_init()

    def __len__(self):
        return self.tot_frame_num

    def get_frame_video_idx(self, i):
        return self.loader.get_parent_index(i)
    
    def get_frame_video_idx_range(self, beg, end):
        res = []
        for i in range(beg, end):
            res.append(self.loader.get_parent_index(i))
        return res

    def get_frame_addr(self, i):
        return self.loader.moment(i)[0]

    def get_local_index(self, i):
        return self.loader.get_local_index(i)

    def _dataset_init(self):
        def subdir_loader(dir):
            if self.file_format == '.mp4':
                return load.DirLoader(dir, load.VideoLoader)
            elif self.file_format == '.npy':
                return load.DirLoader(dir, lambda d: load.ResourceDirLoader(d, np.load))

        self.loader = load.DirLoader(self.dir, subdir_loader)
        self.tot_frame_num = len(self.loader)
        self.start = 0
        self.end = self.tot_frame_num
        self.last_valid_frame = None
    
    def __getitem__(self, indice):
        frame_range = self._context_range(indice=indice)
        img_batch = []

        frames = self.loader.get_indices(frame_range)
        for frame in frames:
            if frame is not None:
                self.last_valid_frame = frame
            else:
                frame = self.last_valid_frame
            # [h,w,c] -> [c,h,w] BGR
            cur_img = np.transpose(frame, [2, 0, 1])
            img_batch.append(cur_img)

        img_batch = np.array(img_batch)

        if self.all_bboxes is not None:
            # cropping
            img_batch = get_foreground(img=img_batch, bboxes=self.all_bboxes[indice], patch_size=self.patch_size)
        img_batch = torch.from_numpy(img_batch)  # [num_bboxes,frames_num,C,patch_size, patch_size]

        if self.combine_datasets is not None:
            combined = [img_batch]
            for d in self.combine_datasets:
                f = d[indice][0]
                combined.append(f)
            combined.append(torch.zeros(1))
            return combined
        else:
            return img_batch, torch.zeros(1)


    def __iter__(self):
        worker_info = torch.utils.data.get_worker_info()

        if worker_info is None:
            iter_start = self.start
            iter_end = self.end
        else:
            per_worker = int(math.ceil((self.end - self.start) / float(worker_info.num_workers)))
            worker_id = worker_info.id
            iter_start = self.start + worker_id * per_worker
            iter_end = min(iter_start + per_worker, self.end)
    
        def gen(beg, end):
            for indice in range(beg, end):
                yield self[indice]
    
        return iter(gen(iter_start, iter_end))
