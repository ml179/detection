import cv2
import os
import numpy as np

img_root = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/flows/Train'

def show_img(im):
    cv2.imshow('im', im)
    while True:
        key = cv2.waitKey(1) & 0xff
        if key==ord('w'):
            break
        elif key==ord('q'):
            exit()

for img in os.listdir(img_root):
    path = f'{img_root}/{img}'
    im = np.load(path)
    im = cv2.normalize(im, None, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)
    im = im[:,:, 0]
    show_img(im)