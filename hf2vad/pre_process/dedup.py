import cv2
import os
import numpy as np

img_root = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/frames/Train1'

def dhash(image, hashSize=8):
	# convert the image to grayscale and resize the grayscale image,
	# adding a single column (width) so we can compute the horizontal
	# gradient
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    resized = cv2.resize(gray, (hashSize + 1, hashSize))
    # compute the (relative) horizontal gradient between adjacent
    # column pixels
    diff = resized[:, 1:] > resized[:, :-1]
    # convert the difference image to a hash and return it
    return sum([2 ** i for (i, v) in enumerate(diff.flatten()) if v]) // 1000

hashes = {}
threshold=100.
image0 = None

for img in os.listdir(img_root):
    path = f'{img_root}/{img}'
    image = cv2.imread(path)

    h = dhash(image)
    p = hashes.get(h, [])
    p.append(path)
    hashes[h] = p

    if image0 is not None and np.sum( np.absolute(image-image0) )/np.size(image) < threshold:
        #print(path, np.sum( np.absolute(image-image0) )/np.size(image))
        pass

    image0 = image

for k, v in hashes.items():
    print(k, v)