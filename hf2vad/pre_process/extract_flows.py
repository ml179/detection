import context as _
import os
from  pre_process.flownet_networks.flownet2_models import FlowNet2
from dataset import VideoDataset
import numpy as np
import torch
from model import process_model, config_app

CFG = {
    "model_path": "hf2vad/assets/FlowNet2_checkpoint.pth.tar"
}

class FlowNetModel:    
    def __init__(self):
        self.model = FlowNet2()
        pretrained_dict = torch.load(CFG['model_path'])['state_dict']
        model_dict = self.model.state_dict()
        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        model_dict.update(pretrained_dict)
        self.model.load_state_dict(model_dict)
        self.model.cuda()
    
    def initialize(self, save_dir, dataset):
        self.dataset = dataset
        self.save_dir = save_dir

    def prepare_batch(self, batch, _, batch_ds_offset):
        cur_imgs = np.transpose(batch.numpy(), [1, 0, 3, 4, 2])  # [#frames,3,h,w] -> [#frames,h,w,3]

        im1 = cur_imgs[0]
        im2 = cur_imgs[1]
        # [0-255]
        ims = np.array([im1, im2]).astype(np.float32)  # [2,h',w',3]
        ims = np.transpose(ims, [1, 0, 2, 3, 4])
        ims = torch.from_numpy(ims)
        ims = ims.permute(0, 4, 1, 2, 3).contiguous().cuda()  # [bs,2,H,W,img_channel] -> [bs,img_channel,2,H,W]
        return ims
    
    def predict(self, batch, batch_ds_offset):
        return self.model(batch).cpu().data

    def prepare_predicts(self, preds, batch_ds_offset):
        for i in range(len(preds)):
            idx = batch_ds_offset + i
            cur_img_addr = self.dataset.get_frame_addr(idx)
            video_of_path = os.path.join(self.save_dir, cur_img_addr.split('/')[-2], cur_img_addr.split('/')[-1])
            local_idx = self.dataset.get_local_index(idx)

            if os.path.exists(video_of_path) is False:
                os.makedirs(video_of_path, exist_ok=True)

            pred_flow = preds[i].numpy().transpose((1, 2, 0))  # [h',w',2]
            np.save(os.path.join(video_of_path, f"{local_idx:04d}.npy"), pred_flow)

    def finalize(self):
        print('flows saved')

if __name__ == '__main__':
    workdir = config_app()
    dataset = VideoDataset(f'{workdir}/videos', context_frame_num=1)
    model = FlowNetModel(f'{workdir}/flows', dataset)
    process_model(model, dataset, background_saving=True, num_workers=8)
