import pickle as pkl
import numpy as np
import cv2

with open('/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/ped2_bboxes_train.pkl', mode='rb') as f:
    bboxes = pkl.load(f)

img_root = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/frames/Train'

def show_img(im):
    cv2.imshow('im', im)
    while True:
        key = cv2.waitKey(1) & 0xff
        if key==ord('w'):
            break
        elif key==ord('q'):
            exit()

for i, img in enumerate(bboxes):
    i = i + 1
    im = cv2.imread(f'{img_root}/{i:04d}.jpg')

    for box in img:
        box = box.astype(np.uint16)
        print(box)
        im = cv2.rectangle(im, (box[0], box[1]), (box[2], box[3]), color=(255, 0, 0))

    show_img(im)