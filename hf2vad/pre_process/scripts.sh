ffmpeg -i trimmed.mp4 -vf "fps=5,crop=750:720:300:0,scale=256:256" vidf.mp4
ffmpeg -i vidf.mp4 -qscale:v 1 -qmin 1 frames/Train/%04d.jpg
python3.8 extract_flows.py --proj_root=/mnt/c/Users/binarycat/YandexDisk/FishMl --dataset_name=ped2 --mode=train