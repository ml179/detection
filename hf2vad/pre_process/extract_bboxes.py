import context as _
import numpy as np
import cv2
import torch
import pickle as pkl
from hf2vad.dataset import VideoDataset
from hf2vad.utils.vis_utils import img_batch_tensor2numpy
from pre_process.mmdet_utils import init_detector, inference_detector
from model import process_model, config_app


torch.backends.cudnn.benchmark = True
torch.backends.cudnn.enabled = True


CFG = {
    #"mm_det_config_file": 'assets/latest_version_cascade_rcnn_r101_fpn_1x.py',
    #"mm_det_ckpt_file": 'assets/cascade_rcnn_r101_fpn_1x_coco_20200317-0b6a2fbf.pth',
    "mm_det_config_file": 'hf2vad/assets/yolox.py',
    "mm_det_ckpt_file": 'hf2vad/assets/yolox.pth',
    "conf_thr": 0.00,
    "min_area": 50 * 50,
    "cover_thr": 0.7,
    "binary_thr": 15,
    "gauss_mask_size": 5,
    'contour_min_area': 50 * 50
}

def fill_zeros(r, num_boxes):
    if len(r) < num_boxes:
        pad_count = num_boxes - len(r)
        z = np.zeros((pad_count, 4), dtype=r.dtype)
        r = np.concatenate([r, z], axis=0)
    else:
        r = r[:num_boxes]
    return r

def getObjBboxes(imgs, model, num_boxes):
    results = []
    predicts = inference_detector(model, [im for im in imgs])

    for pred in predicts:
        bbox_result = np.vstack(pred)
        bboxes = []

        for b in bbox_result:
            if b.sum() > 0 and (b[2] - b[0]) < 128 and (b[3] - b[1]) < 128:
                bboxes.append(b)

        if len(bboxes) > 0:
            bboxes = np.array(bboxes)

            scores = bboxes[:, -1]  # x1,y1,x2,y2,class_score
            bboxes = bboxes[scores > CFG["conf_thr"], :]

            x1 = bboxes[:, 0]
            y1 = bboxes[:, 1]
            x2 = bboxes[:, 2]
            y2 = bboxes[:, 3]
            bbox_areas = (y2 - y1 + 1) * (x2 - x1 + 1)
            r = bboxes[bbox_areas >= CFG["min_area"], :4]
            r = delCoverBboxes(r)
            r = fill_zeros(r, num_boxes)
            results.append(r)
        else:
            results.append(np.zeros(shape=(num_boxes, 4)))
    
    results = np.array(results, dtype=np.float32)
    return results


def delCoverBboxes(bboxes):
    assert bboxes.ndim == 2
    assert bboxes.shape[1] == 4

    x1 = bboxes[:, 0]
    y1 = bboxes[:, 1]
    x2 = bboxes[:, 2]
    y2 = bboxes[:, 3]
    bbox_areas = (y2 - y1 + 1) * (x2 - x1 + 1)

    sort_idx = bbox_areas.argsort()  # Index of bboxes sorted in ascending order by area size

    keep_idx = []
    for i in range(sort_idx.size):  # calculate overlap with i-th bbox
        # Calculate the point coordinates of the intersection
        x11 = np.maximum(x1[sort_idx[i]], x1[sort_idx[i + 1:]])
        y11 = np.maximum(y1[sort_idx[i]], y1[sort_idx[i + 1:]])
        x22 = np.minimum(x2[sort_idx[i]], x2[sort_idx[i + 1:]])
        y22 = np.minimum(y2[sort_idx[i]], y2[sort_idx[i + 1:]])
        # Calculate the intersection area
        w = np.maximum(0, x22 - x11 + 1)
        h = np.maximum(0, y22 - y11 + 1)
        overlaps = w * h

        ratios = overlaps / bbox_areas[sort_idx[i]]
        num = ratios[ratios > CFG["cover_thr"]]
        if num.size == 0:
            keep_idx.append(sort_idx[i])

    return bboxes[keep_idx]


def getFgBboxes(img_batch, bboxes, num_boxes):
    area_thr = CFG["contour_min_area"]
    binary_thr = CFG["binary_thr"]
    gauss_mask_size = CFG["gauss_mask_size"]
    extend = 2

    sum_grad = 0
    for i in range(img_batch.shape[0] - 1):
        img1 = img_batch[i, :, :, :]
        img2 = img_batch[i + 1, :, :, :]
        img1 = cv2.GaussianBlur(img1, (gauss_mask_size, gauss_mask_size), 0)
        img2 = cv2.GaussianBlur(img2, (gauss_mask_size, gauss_mask_size), 0)

        grad = cv2.absdiff(img1, img2)
        sum_grad = grad + sum_grad

    sum_grad = cv2.threshold(sum_grad, binary_thr, 255, cv2.THRESH_BINARY)[1]  # temporal gradient

    for bbox in bboxes:
        bbox_int = bbox.astype(np.int32)
        extend_y1 = np.maximum(0, bbox_int[1] - extend)
        extend_y2 = np.minimum(bbox_int[3] + extend, sum_grad.shape[0])
        extend_x1 = np.maximum(0, bbox_int[0] - extend)
        extend_x2 = np.minimum(bbox_int[2] + extend, sum_grad.shape[1])
        sum_grad[extend_y1:extend_y2 + 1, extend_x1:extend_x2 + 1] = 0

    sum_grad = cv2.cvtColor(sum_grad, cv2.COLOR_BGR2GRAY)
    contours, _ = cv2.findContours(sum_grad, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    fg_bboxes = []
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        sum_grad = cv2.rectangle(sum_grad, (x, y), (x + w, y + h), color=255, thickness=1)
        area = (w + 1) * (h + 1)
        if area > area_thr and w / h < 10 and h / w < 10:
            extend_x1 = np.maximum(0, x - extend)
            extend_y1 = np.maximum(0, y - extend)
            extend_x2 = np.minimum(x + w + extend, sum_grad.shape[1])
            extend_y2 = np.minimum(y + h + extend, sum_grad.shape[0])
            fg_bboxes.append([extend_x1, extend_y1, extend_x2, extend_y2])

    fg_bboxes = np.array(fg_bboxes, dtype=np.float32)
    fg_bboxes = fill_zeros(fg_bboxes, num_boxes)
    return np.array(fg_bboxes)


class MMDetModel:
    def __init__(self):
        self.all_bboxes = list()
        self.model = init_detector(CFG["mm_det_config_file"], CFG["mm_det_ckpt_file"], device="cuda:0")
    
    def initialize(self, save_dir, num_boxes):
        self.num_boxes = num_boxes
        self.save_dir = save_dir

    def prepare_batch(self, batch, _, batch_ds_offset):
        return img_batch_tensor2numpy(batch)
    
    def predict(self, batch, batch_ds_offset):
        batch_bboxes = getObjBboxes(batch[:, 1, ...], self.model, self.num_boxes)
        fgBboxes = []

        for i in range(len(batch)):
            fgBboxes.append(getFgBboxes(batch[i], batch_bboxes[i], self.num_boxes))

        return batch_bboxes, fgBboxes

    def prepare_predicts(self, pred, batch_ds_offset):
        for obj_bboxes, fg_bboxes in zip(pred[0], pred[1]):
            if fg_bboxes.shape[0] > 0:
                cur_bboxes = np.concatenate((obj_bboxes, fg_bboxes), axis=0)
            else:
                cur_bboxes = obj_bboxes

            self.all_bboxes.append(cur_bboxes)
    
    def finalize(self):
        with open(self.save_dir, mode='wb') as f:
            pkl.dump(self.all_bboxes, f)
            print('bboxes saved!')


if __name__ == '__main__':
    workdir = config_app()
    dataset = VideoDataset(f'{workdir}/videos', context_frame_num=1)
    model = MMDetModel(f'{workdir}/bboxes.pkl', 10)
    process_model(model, dataset, batch_size=10)
