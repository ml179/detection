import os
import sys

if os.path.basename(os.getcwd()) != 'hf2vad':
    os.chdir('../')

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '../..')))

