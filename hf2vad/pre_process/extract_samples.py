import context as _
import os
import numpy as np
import joblib
import pickle as pkl
from hf2vad.utils.vis_utils import img_batch_tensor2numpy
from hf2vad.dataset import VideoDataset
from model import process_model, config_app


CFG = {
    "num_predicted_frame": 1,
    "num_samples_each_chunk": 100000
}


class SamplerModel:
    def __init__(self):
        self.global_sample_id = 0
        self.chunked_samples = dict(sample_id=[], appearance=[], motion=[], bbox=[], pred_frame=[])
    
    def initialize(self, save_dir, dataset, all_bboxes):
        self.all_bboxes = all_bboxes
        self.dataset = dataset
        self.save_dir = save_dir

    def prepare_batch(self, batches, flow_batches, _, batch_ds_offset):
        for icnt in range(len(batches)):
            idx = batch_ds_offset + icnt

            frameRange = self.dataset._context_range(idx)
            cur_bboxes = self.all_bboxes[idx]

            batch = batches[icnt]
            flow_batch = flow_batches[icnt]

            for i, b in enumerate(cur_bboxes):
                if b.sum() == 0:
                    break
            num_valid_boxes = i

            if num_valid_boxes > 0:
                batch = img_batch_tensor2numpy(batch)
                flow_batch = img_batch_tensor2numpy(flow_batch)

                # each STC treated as a sample
                for idx_box in range(min(num_valid_boxes, len(batch))): 
                    self.chunked_samples["sample_id"].append(self.global_sample_id)
                    self.chunked_samples["appearance"].append(batch[idx_box])
                    self.chunked_samples["motion"].append(flow_batch[idx_box])
                    self.chunked_samples["bbox"].append(cur_bboxes[idx_box])
                    self.chunked_samples["pred_frame"].append(frameRange[-CFG["num_predicted_frame"]:])  # the frame id of last patch
                    self.global_sample_id += 1

    def predict(self, batch, batch_ds_offset):
        pass

    def prepare_predicts(self, preds, batch_ds_offset):
        pass

    def finalize(self):
        self.chunked_samples["sample_id"] = np.array(self.chunked_samples["sample_id"])
        self.chunked_samples["appearance"] = np.array(self.chunked_samples["appearance"])
        self.chunked_samples["motion"] = np.array(self.chunked_samples["motion"])
        self.chunked_samples["bbox"] = np.array(self.chunked_samples["bbox"])
        self.chunked_samples["pred_frame"] = np.array(self.chunked_samples["pred_frame"])
        joblib.dump(self.chunked_samples, self.save_dir)
        print("Chunk file saved!")


if __name__ == '__main__':
    workdir = config_app()

    bbox_path = f'{workdir}/bboxes.pkl'
    save_dir = f'{workdir}/chunked_samples.pkl'
    with open(bbox_path, 'rb') as f:
        all_bboxes = pkl.load(f)

    flows_dataset = VideoDataset(f'{workdir}/flows', context_frame_num=4, file_format='.npy',
                                 border_mode="predict", all_bboxes=all_bboxes, patch_size=32)

    videos_dataset = VideoDataset(f'{workdir}/videos', context_frame_num=4, combine_datasets=[flows_dataset],
                                  border_mode="predict", all_bboxes=all_bboxes, patch_size=32)

    model = SamplerModel(save_dir, videos_dataset, all_bboxes)
    process_model(model, videos_dataset, batch_size=10)
