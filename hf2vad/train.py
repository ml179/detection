import context as _
import yaml
import pickle as pkl

from sampled_dataset import ChunkeSampleDataset
from model import train_model, config_app, build_model, HVVADModel, MemAeModel

EPOCHS = 30

def train(workdir, Model, cfg, last_flow=False):
    config = yaml.safe_load(open(cfg))
    chunked_samples_file = f'{workdir}/chunked_samples.pkl'
    sample_dataset = ChunkeSampleDataset(chunked_samples_file, last_flow=last_flow)

    model = build_model(Model, config, 'train')
    model.initialize(sample_dataset)
    train_model(model, sample_dataset, EPOCHS, batch_size=128)


if __name__ == '__main__':
    workdir = config_app()

    train(workdir, MemAeModel, "./hf2vad/cfgs/ml_memAE_sc_cfg.yaml", last_flow=True)
    train(workdir, HVVADModel, "./hf2vad/cfgs/cfg.yaml")
