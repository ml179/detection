import context as _
import pickle as pkl
import yaml

from pre_process.extract_bboxes import MMDetModel
from pre_process.extract_flows import FlowNetModel
from pre_process.extract_samples import SamplerModel 
from sampled_dataset import ChunkeSampleDataset
from model import process_model, config_app, build_model, HVVADModel
from dataset import VideoDataset


if __name__ == '__main__':
    workdir = config_app()
    bboxesModel = MMDetModel()
    flowModel = FlowNetModel()
    samplerModel = SamplerModel()
    config = yaml.safe_load(open("./hf2vad/cfgs/cfg.yaml"))
    anomalyModel = build_model(HVVADModel, config, mode='test')
    print('Models initialized')

    videos_dataset = VideoDataset(f'{workdir}/videos', context_frame_num=1)

    # bboxes extraction
    bboxes_path = f'{workdir}/bboxes.pkl'
    bboxesModel.initialize(bboxes_path, 10)
    process_model(bboxesModel, videos_dataset, batch_size=10)

    with open(bboxes_path, 'rb') as f:
        all_bboxes = pkl.load(f)

    # flows extraction
    flowModel.initialize(f'{workdir}/flows', videos_dataset)
    process_model(flowModel, videos_dataset, background_saving=True)

    flows_dataset = VideoDataset(f'{workdir}/flows', context_frame_num=4, file_format='.npy',
                                 border_mode="predict", all_bboxes=all_bboxes, patch_size=32)

    videos_flows_dataset = VideoDataset(f'{workdir}/videos', context_frame_num=4, combine_datasets=[flows_dataset],
                                  border_mode="predict", all_bboxes=all_bboxes, patch_size=32)

    # samples extraction
    samples_path = f'{workdir}/chunked_samples.pkl'
    samplerModel.initialize(samples_path, videos_flows_dataset, all_bboxes)
    process_model(samplerModel, videos_flows_dataset, batch_size=10)

    sample_dataset = ChunkeSampleDataset(samples_path)

    # anomaly detection
    anomalyModel.initialize(len(videos_dataset))
    results = process_model(anomalyModel, sample_dataset, batch_size=128)

    # saving
    results_path = f'{workdir}/results.pkl'
    with open(results_path, 'wb') as f:
        pkl.dump(results, f)
    print('Results saved')
