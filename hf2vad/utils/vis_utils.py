from utils.flow_utils import flow2img
import torch
import cv2
import matplotlib.pyplot as plt
import numpy as np

def img_tensor2numpy(img):
    # mutual transformation between ndarray-like imgs and Tensor-like images
    # both intensity and rgb images are represented by 3-dim data
    if isinstance(img, np.ndarray):
        return torch.from_numpy(np.transpose(img, [2, 0, 1]))
    else:
        return np.transpose(img, [1, 2, 0]).numpy()


def img_batch_tensor2numpy(img_batch):
    # both intensity and rgb image batch are represented by 4-dim data
    if isinstance(img_batch, np.ndarray):
        if len(img_batch.shape) == 4:
            return torch.from_numpy(np.transpose(img_batch, [0, 3, 1, 2]))
        else:
            return torch.from_numpy(np.transpose(img_batch, [0, 1, 4, 2, 3]))
    else:
        if len(img_batch.numpy().shape) == 4:
            return np.transpose(img_batch, [0, 2, 3, 1]).numpy()
        else:
            return np.transpose(img_batch, [0, 1, 3, 4, 2]).numpy()


# plt.switch_backend('agg')

def visualize_sequences(batch, seq_len, return_fig=True):
    """
    visualize a sequence (imgs or flows)
    """
    sequences = []
    channels_per_frame = batch.shape[-1] // seq_len
    for i in range(batch.shape[0]):
        cur_sample = batch[i]  # [H,W,channels_per_frame * seq_len]
        if channels_per_frame == 2:
            sequence = [flow2img(cur_sample[:, :, j * channels_per_frame:(j + 1) * channels_per_frame])
                        for j in range(seq_len)]
        else:
            # to RGB
            sequence = [cur_sample[:, :, j * channels_per_frame:(j + 1) * channels_per_frame][:, :, ::-1]
                        for j in range(seq_len)]
        sequences.append(np.hstack(sequence))
    sequences = np.vstack(sequences)

    if return_fig:
        fig = plt.figure()
        plt.imshow(sequences)
        return fig
    else:
        return sequences
