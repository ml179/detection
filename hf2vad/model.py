import math
import threading
import torch
import argparse
import os
import numpy as np
import torch.nn as nn
import gc
import shutil
from tqdm import tqdm
from models.ml_memAE_sc import ML_MemAE_SC
from torch import optim

from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter

from losses.loss import Gradient_Loss, Intensity_Loss, aggregate_kl_loss
from models.mem_cvae import HFVAD
from utils.vis_utils import img_batch_tensor2numpy, visualize_sequences
from utils.initialization_utils import weights_init_kaiming
from utils.model_utils import loader, saver, only_model_saver


class HVVADModel:
    def __init__(self, config, mode, model_path="./hf2vad/ckpt/ped2_ML_MemAE_SC_CVAE/best.pth") -> None:
        self.mode = mode
        self.config = config
        self.model = HFVAD(num_hist=config["model_paras"]["clip_hist"],
                    num_pred=config["model_paras"]["clip_pred"],
                    config=config,
                    features_root=config["model_paras"]["feature_root"],
                    num_slots=config["model_paras"]["num_slots"],
                    shrink_thres=config["model_paras"]["shrink_thres"],
                    mem_usage=config["model_paras"]["mem_usage"],
                    skip_ops=config["model_paras"]["skip_ops"],
        ).to(config["device"]).eval()

        self.device = config["device"]
        self.score_func = nn.MSELoss(reduction="none")

        if mode == 'test':
            model_weights = torch.load(model_path)["model_state_dict"]
            self.model.load_state_dict(model_weights)
        else:
            ML_MemAE_SC_state_dict = torch.load(config["ML_MemAE_SC_pretrained"])["model_state_dict"]
            self.model.memAE.load_state_dict(ML_MemAE_SC_state_dict)

            for param in self.model.memAE.parameters():
                param.requires_grad = False
            self.model.memAE.eval()

            self.optimizer = optim.Adam(self.model.vunet.parameters(), lr=config["lr"], eps=1e-7, weight_decay=0.0)
            self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, step_size=50, gamma=0.8)

            self.intensity_loss = Intensity_Loss(l_num=config["intensity_loss_norm"]).to(self.device)
            self.grad_loss = Gradient_Loss(
                config["alpha"],
                config["model_paras"]["img_channels"] * config["model_paras"]["clip_pred"],
            self.device).to(self.device)

            if not config["pretrained"]:
                self.model.vunet.apply(weights_init_kaiming)
            else:
                assert (config["pretrained"] is not None)
                model_state_dict, optimizer_state_dict, self.step = loader(config["pretrained"])
                self.model.load_state_dict(model_state_dict)
                self.optimizer.load_state_dict(optimizer_state_dict)

    def predict(self, sample_frames_test, sample_ofs_test):
        sample_frames_test = sample_frames_test.to(self.device)
        sample_ofs_test = sample_ofs_test.to(self.device)

        if self.mode == 'test':
            out = self.model(sample_frames_test, sample_ofs_test, mode=self.mode)    

            loss_of_test = self.score_func(out["of_recon"], out["of_target"]).cpu().data.numpy()
            loss_frame_test = self.score_func(out["frame_pred"], out["frame_target"]).cpu().data.numpy()

            of_scores = np.sum(np.sum(np.sum(loss_of_test, axis=3), axis=2), axis=1)
            frame_scores = np.sum(np.sum(np.sum(loss_frame_test, axis=3), axis=2), axis=1)

            scores = self.config["w_r"] * of_scores + self.config["w_p"] * frame_scores
            return out, scores
        else:
            self.model.vunet.train()
            out = self.model(sample_frames_test, sample_ofs_test, mode=self.mode)

            loss_kl = aggregate_kl_loss(out["q_means"], out["p_means"])
            loss_frame = self.intensity_loss(out["frame_pred"], out["frame_target"])
            loss_grad = self.grad_loss(out["frame_pred"], out["frame_target"])

            loss_all = self.config["lam_kl"] * loss_kl + \
                self.config["lam_frame"] * loss_frame + \
                self.config["lam_grad"] * loss_grad

            self.optimizer.zero_grad()
            loss_all.backward()
            self.optimizer.step()
            return out, loss_all
    

class MemAeModel:
    def __init__(self, config, mode, model_path="./hf2vad/ckpt/ped2_ML_MemAE_SC/best.pth"):
        self.config = config
        self.mode = mode
        self.model = ML_MemAE_SC(num_in_ch=config["model_paras"]["motion_channels"],
                            seq_len=config["model_paras"]["num_flows"],
                            features_root=config["model_paras"]["feature_root"],
                            num_slots=config["model_paras"]["num_slots"],
                            shrink_thres=config["model_paras"]["shrink_thres"],
                            mem_usage=config["model_paras"]["mem_usage"],
                            skip_ops=config["model_paras"]["skip_ops"]).to(config["device"]).eval()
        self.device = config["device"]

        if mode == 'test':
            model_weights = torch.load(model_path)["model_state_dict"]
            self.model.load_state_dict(model_weights)
            self.score_func = nn.MSELoss(reduction="none")
        else:
            self.optimizer = optim.Adam(self.model.parameters(), lr=config["lr"], eps=1e-7, weight_decay=0.0)
            self.scheduler = optim.lr_scheduler.MultiStepLR(self.optimizer, milestones=[50], gamma=0.8)
            self.score_func = nn.MSELoss().to(self.device)
            
            if not config["pretrained"]:
                self.model.apply(weights_init_kaiming)
            else:
                assert (config["pretrained"] is not None)
                model_state_dict, optimizer_state_dict, step = loader(config["pretrained"])
                self.model.load_state_dict(model_state_dict)
                self.optimizer.load_state_dict(optimizer_state_dict)
    
    def predict(self, sample_frames, sample_ofs):
        sample_ofs = sample_ofs.cuda()
        if self.mode == 'test':
            out = self.model(sample_ofs)
            loss_of_test = self.score_func(out["recon"], sample_ofs).cpu().data.numpy()
            scores = np.sum(np.sum(np.sum(loss_of_test, axis=3), axis=2), axis=1)
            return out, scores
        else:
            self.model.train()
            out = self.model(sample_ofs)

            loss_recon = self.score_func(out["recon"], sample_ofs)
            loss_sparsity = (
                    torch.mean(torch.sum(-out["att_weight3"] * torch.log(out["att_weight3"] + 1e-12), dim=1))
                    + torch.mean(torch.sum(-out["att_weight2"] * torch.log(out["att_weight2"] + 1e-12), dim=1))
                    + torch.mean(torch.sum(-out["att_weight1"] * torch.log(out["att_weight1"] + 1e-12), dim=1))
            )

            loss_all = self.config["lam_recon"] * loss_recon + self.config["lam_sparse"] * loss_sparsity

            self.optimizer.zero_grad()
            loss_all.backward()
            self.optimizer.step()
            return out, loss_all


class DetectionModel:
    def __init__(self, config, model, stats_save_path=None):
        self.model = model
        self.config = config
        self.stats_save_path = stats_save_path
        self.training_stats = []

        eval_dir = os.path.join(self.config["eval_root"], self.config["exp_name"])
        os.makedirs(eval_dir, exist_ok=True)

        training_stats_path = os.path.join("./hf2vad/eval", self.config["exp_name"], "training_stats.npy")
        self.training_scores_stats = None
        if os.path.exists(training_stats_path):
            self.training_scores_stats = torch.load(training_stats_path)
            self.scores_mean = np.mean(self.training_scores_stats) 
            self.scores_std = np.std(self.training_scores_stats)
   
    def initialize(self, num_frames):
        self.testset_num_frames = num_frames
        self.frame_bbox_scores = [{} for i in range(self.testset_num_frames)]
        self.frame_bboxes = [{} for i in range(self.testset_num_frames)]

    def prepare_batch(self, sample_frames_test, sample_ofs_test, bbox_test, pred_frame_test, indices_test, batch_ds_offset):
        return sample_frames_test, sample_ofs_test, pred_frame_test, bbox_test
    
    def predict(self, prep, batch_ds_offset):
        sample_frames_test, sample_ofs_test, pred_frame_test, bbox_test = prep
        out, scores = self.model.predict(sample_frames_test, sample_ofs_test)
        return pred_frame_test, bbox_test, out, scores

    def prepare_predicts(self, preds, batch_ds_offset):
        pred_frame_test, bbox_test, out, scores = preds

        if self.training_scores_stats is not None:
            scores = (scores - self.scores_mean) / self.scores_std

        # anomaly scores for each sample
        for i in range(len(scores)):
            self.frame_bbox_scores[pred_frame_test[i][-1].item()][i] = scores[i]
        
        for i in range(len(bbox_test)):
            self.frame_bboxes[pred_frame_test[i][-1].item()][i] = (scores[i], bbox_test[i].tolist())
        
        self.training_stats.append(scores)
        return out, scores

    def finalize(self):
        gc.collect()

        if self.stats_save_path is not None:
            training_stats = np.concatenate(self.training_stats, axis=0)
            torch.save(training_stats, self.stats_save_path)

        frames_results = []
        for fi, boxes in enumerate(self.frame_bboxes):
            if len(boxes) > 0:
                a_boxes = []
                for _, s in boxes.items():
                    a_boxes.append(s)
                frames_results.append((fi, a_boxes))
    
        frames_results = sorted(frames_results, key=lambda r: r[0])
        frames_results = list(zip(*frames_results))[1:]

        return frames_results[0]


def process_model(model, dataset, num_workers=20, batch_size=None, background_saving=False):
    threads = []
    with torch.no_grad():
        results = []
        scores = []
        if batch_size is None:
            batch_size = math.ceil(len(dataset) / num_workers)
        dataset_loader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=False, num_workers=num_workers)

        batch_offset = 0
        for _, batch in tqdm(enumerate(dataset_loader), total=len(dataset_loader)):        
            prep_batch = model.prepare_batch(*batch, batch_offset)
            preds = model.predict(prep_batch, batch_offset)
            if background_saving:
                t = threading.Thread(target=model.prepare_predicts, args=(preds, batch_offset))
                t.start()
                threads.append(t)
            else:
                rs = model.prepare_predicts(preds, batch_offset)
                if rs is not None:
                    r, s = rs
                    results.append(r)
                    s = np.mean(s)
                    scores.append(s)
            batch_offset += len(batch[0])
        
        for t in threads:
            t.join()

        f = model.finalize()

        scores = np.array(scores)
        print('Model score: ', scores.mean())

        if f is not None:
            return f
        elif len(results) > 0:
            return torch.Tensor(results)


class DetectionModelTrain:
    def __init__(self, config, model):
        self.model = model
        self.config = config

        self.paths = dict(log_dir="%s/%s" % (config["log_root"], config["exp_name"]),
                 ckpt_dir="%s/%s" % (config["ckpt_root"], config["exp_name"]))
        self.writer = SummaryWriter(self.paths["log_dir"])
        shutil.copyfile("./hf2vad/cfgs/cfg.yaml", os.path.join(config["log_root"], config["exp_name"], "cfg.yaml"))
    
    def initialize(self, dataset):
        self.dataset = dataset

    def prepare_batch(self, sample_frames_test, sample_ofs_test, bbox_test, pred_frame_test, indices_test, batch_ds_offset):
        return sample_frames_test, sample_ofs_test, pred_frame_test, bbox_test

    def train_step(self, prep, epoch, step, batch_ds_offset):
        sample_frames, sample_ofs, pred_frame_test, bbox_test = prep
        out, loss_all = self.model.predict(sample_frames, sample_ofs)
        return out, sample_frames, sample_ofs, loss_all
   
    def end_step(self, preds, epoch, step, batch_ds_offset):
        out, sample_frames, sample_ofs, loss_all = preds

        if step % self.config["logevery"] == self.config["logevery"] - 1:
            print("[Step: {}/ Epoch: {}]: Loss: {:.4f} ".format(step + 1, epoch + 1, loss_all))

            self.writer.add_scalar('loss_total/train', loss_all, global_step=step + 1)

            num_vis = 6
            self.writer.add_figure("img/train_sample_frames",
                                visualize_sequences(img_batch_tensor2numpy(
                                    sample_frames.cpu()[:num_vis, :, :, :]),
                                    seq_len=sample_frames.size(1) // 3,
                                    return_fig=True),
                                global_step=step + 1)
            self.writer.add_figure("img/train_frame_recon",
                                visualize_sequences(img_batch_tensor2numpy(
                                    out["frame_pred"].detach().cpu()[:num_vis, :, :, :]),
                                    seq_len=self.config["model_paras"]["clip_pred"],
                                    return_fig=True),
                                global_step=step + 1)

            self.writer.add_figure("img/train_of_target",
                                visualize_sequences(img_batch_tensor2numpy(
                                    sample_ofs.cpu()[:num_vis, :, :, :]),
                                    seq_len=sample_ofs.size(1) // 2,
                                    return_fig=True),
                                global_step=step + 1)
            self.writer.add_figure("img/train_of_recon",
                                visualize_sequences(img_batch_tensor2numpy(
                                    out["of_recon"].detach().cpu()[:num_vis, :, :, :]),
                                    seq_len=sample_ofs.size(1) // 2,
                                    return_fig=True),
                                global_step=step + 1)

            self.writer.add_scalar('learning_rate', self.scheduler.get_last_lr()[0], global_step=step + 1)
    
    def train_epoch(self, epoch, step):
        self.model.scheduler.step()
        if epoch % self.config["saveevery"] == self.config["saveevery"] - 1:
            model_save_path = os.path.join(self.paths["ckpt_dir"], self.config["model_savename"])
            saver(self.model.model.state_dict(), self.model.optimizer.state_dict(), model_save_path, epoch + 1, step, max_to_save=5)

            # computer training stats
            #stats_save_path = os.path.join(self.paths["ckpt_dir"], "training_stats.npy-%d" % (epoch + 1))
            stats_save_path = os.path.join(self.paths["ckpt_dir"], "training_stats.npy")

            model = build_model(type(self.model), self.config, 'test', model_save_path, stats_save_path)
            model.initialize(len(self.dataset))
            process_model(model, self.dataset, batch_size=128)

    def finalize(self):
        only_model_saver(self.model.model.state_dict(), os.path.join(self.paths["ckpt_dir"], "best.pth"))


def build_model(Model, config, mode, model_path=None, stats_save_path=None):
    if model_path is None:
        model_core = Model(config, mode)
    else:
        model_core = Model(config, mode, model_path=model_path)

    if mode == 'test':
        return DetectionModel(config, model_core, stats_save_path=stats_save_path)
    else:
        return DetectionModelTrain(config, model_core)


def train_model(model, dataset, num_epochs, num_workers=20, batch_size=None):
    if batch_size is None:
        batch_size = math.ceil(len(dataset) / num_workers)
        
    for epoch in range(num_epochs):
        dataset_loader = DataLoader(dataset=dataset, batch_size=batch_size, shuffle=True, num_workers=num_workers)

        batch_offset = 0
        for step, batch in tqdm(enumerate(dataset_loader), total=len(dataset_loader)):        
            prep_batch = model.prepare_batch(*batch, batch_offset)
            preds = model.train_step(prep_batch, epoch, step, batch_offset)
            model.end_step(preds, epoch, step, batch_offset)
            batch_offset += len(batch[0])
        model.train_epoch(epoch, step)
        del dataset_loader
        
    model.finalize()


def config_app():
    parser = argparse.ArgumentParser()
    parser.add_argument("--proj_root", type=str, default="/home/liuzhian/hdd4T/code/hf2vad", help='project root path')
    parser.add_argument("--dataset_name", type=str, default="ped2", help='dataset name')
    parser.add_argument("--mode", type=str, default="train", help='train or test data')

    args = parser.parse_args()

    if args.mode == "train":
        workdir = os.path.join(args.proj_root, "data", args.dataset_name, "training")
    else:
        workdir = os.path.join(args.proj_root, "data", args.dataset_name, "testing")
    
    return workdir
