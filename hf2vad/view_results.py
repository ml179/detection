import context as _
import numpy as np
import cv2
import pickle as pkl
from model import config_app
from utils.vis_utils import img_tensor2numpy
from detection.utils import show_img
from dataset import VideoDataset
import requests
import datetime

DRAW_THRES = 4
ANOMALY_THRES = 7

def draw_bboxes(im, bboxes, thres):
    for s, rect in bboxes:
        rect = np.array(rect, np.uint16)
        if s >= thres:
            l = min(s * 25, 255)
            new_im = im.copy()
            im = cv2.rectangle(new_im, (rect[0], rect[1]), (rect[2], rect[3]), color=(0, 0, 255))
    
    return im

def max_score(bboxes):
    max = -1
    for s, _ in bboxes:
        if s > max:
            max = s
    return max

def send_score(score, thres):
    code = "norm"
    if score >= thres:
        code = "alert"

    r = requests.post(
        "http://localhost/api/predict/createOne",
        data={
            'date': datetime.datetime.now().isoformat(),
            'predictCodeName': code,
            'value': score
        })

    print(r.content)
    print(r.status_code, r.reason)

def show_results_cv(results, videos_dataset):
    for i, bboxes in enumerate(results):
        im = img_tensor2numpy(videos_dataset[i][0][1])
        im = draw_bboxes(im, bboxes, DRAW_THRES)

        score = max_score(bboxes)
        #send_score(score, ANOMALY_THRES)
        show_img(im, wait=False)

if __name__ == '__main__':
    workdir = config_app()
    results_path = f'{workdir}/results.pkl'
    with open(results_path, 'rb') as f:
        results = pkl.load(f)

    videos_dataset = VideoDataset(f'{workdir}/videos', context_frame_num=1)
    show_results_cv(results, videos_dataset)
