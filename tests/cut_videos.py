from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip

videos = ["IPCAM_2023-02-02-18-52-28.mp4", "IPCAM_2023-02-02-18-58-28.mp4", "IPCAM_2023-02-02-19-04-28.mp4",
          "IPCAM_2023-02-02-19-14-28.mp4"]


def cut_video(vid_names):
    timestamps = {"t0": 0, "t1": 15, "t2": 30, "t3": 45, "t4": 59}
    for video in vid_names:
        for i in range(4):
            print(f"{video}{i}.mp4")
            ffmpeg_extract_subclip(f"{video}", timestamps["t" + str(i)], timestamps["t" + str(i+1)],
                                   targetname=f"Cut_{video}_part#{i}.mp4")


cut_video(vid_names=videos)