import context as _
import cv2
import numpy as np
import detection.filter as filter
import detection.load as load
import detection.preprocess as prep
import detection.visualize as visualize
import detection.utils as utils
import detection.dbtools as dbtools
import detection.transform as transform
from detection.load import VideoProccessor
import matplotlib.pyplot as plt
import detection.preprocess as preprocess



def draw_skelets(skelets, src):
    red = np.ones(skelets.shape)
    red = red*255
    src[:,:,1][skelets>0] = red[skelets>0]
    return src 

# pathes = ['data/videos/test/day.mp4', 
#         'data/videos/day/IPCAM_101-05-20230209123015.mp4', 
#         'data/videos/day/IPCAM_2023-02-07-05-46-41.mp4' 
#         'data/videos/day/IPCAM_101-05-20230209123015.mp4']
# path = dbtools.get_the_latest_video()
# path = 'data/videos/day/IPCAM_101-05-20230209123015.mp4'

# def vizualization2(path):
#     source_video = load.VideoLoader(path)
#     prep_video = VideoProccessor(source_video, prep.prepare_frame)
#     xray_video = VideoProccessor (prep_video, transform.xray)
#     gaps_video = VideoProccessor(xray_video, transform.fill_gaps)

#     contours = VideoProccessor([source_video, gaps_video], lambda s,g: transform.filter_contours(s,g, min_area=1500))
#     vis_cont = VideoProccessor ([prep_video,contours], transform.draw_contours)

    
#     return skelets_vis

def add_br(im):
    im= preprocess.apply_brightness_contrast(im, 64, 64)
    im= preprocess.apply_brightness_contrast(im, 64, 64)
    return im

def vizualization(path):
    mean_image = cv2.imread('data/util/mean.jpg')
    mean_image = cv2.cvtColor(mean_image, cv2. COLOR_BGR2GRAY)
    #mean_image = mean_image[110:110+500, 200:200+880]

    source_video = load.VideoLoader(path)
    prep_video = VideoProccessor(source_video, prep.prepare_frame)
    gray_video = VideoProccessor(prep_video, lambda im: cv2.cvtColor(im, cv2.COLOR_BGR2GRAY))

    norm_video = VideoProccessor(gray_video, add_br)
    eq_video = VideoProccessor(norm_video,
        lambda im: cv2.equalizeHist(im.astype(np.uint8)))

    smoothed_gaussian = VideoProccessor(eq_video,
        lambda im: cv2.GaussianBlur(im, (11, 11), 2))

    #mean_image = utils.mean_image(smoothed_gaussian, 0.001)
    dif_video = VideoProccessor(smoothed_gaussian, 
        lambda im: transform.image_dif(im, mean_image))
    dif_video_color = VideoProccessor(dif_video, lambda im: cv2.cvtColor(im, cv2.COLOR_GRAY2BGR))

    thres = VideoProccessor(dif_video, 
        lambda im: cv2.adaptiveThreshold(im, 255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 151, -10))
    morph = VideoProccessor(thres, transform.fill_gaps)
    final = VideoProccessor([prep_video, morph], transform.filter_contours)
    contoured = VideoProccessor([prep_video, final], transform.draw_contours)

    skelets = VideoProccessor(final, transform.erode_skelets)
    contoured = VideoProccessor([contoured, skelets], lambda i, m: transform.draw_contours(i, m, (0, 255, 0), 2))
    #skelets_vis = VideoProccessor([skelets, contoured], draw_skelets)
    return contoured

