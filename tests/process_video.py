import context as _
import cv2
import numpy as np
import detection.filter as filter
import detection.load as load
import detection.preprocess as prep
import detection.visualize as visualize
import detection.utils as utils
import detection.transform as transform
from detection.load import VideoProccessor
import detection.preprocess as preprocess
import matplotlib.pyplot as plt

mean_image = cv2.imread('data/util/mean.jpg')
mean_image = cv2.cvtColor(mean_image, cv2. COLOR_BGR2GRAY)
#mean_image = mean_image[110:110+500, 200:200+880]

def add_br(im):
    im= preprocess.apply_brightness_contrast(im, 64, 64)
    im= preprocess.apply_brightness_contrast(im, 64, 64)
    return im

source_video = load.DirLoader('/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/videos/Train')
prep_video = VideoProccessor(source_video, prep.prepare_frame)
gray_video = VideoProccessor(prep_video, lambda im: cv2.cvtColor(im, cv2.COLOR_BGR2GRAY))

#mean, std = utils.mean_std_metrics(gray_video, 0.001)
#norm_video = VideoProccessor(gray_video, lambda im: utils.normalize((im - mean) / std))
norm_video = VideoProccessor(gray_video, add_br)
eq_video = VideoProccessor(norm_video,
    lambda im: cv2.equalizeHist(im.astype(np.uint8)))
eq_video_color = VideoProccessor(eq_video, lambda im: cv2.cvtColor(im, cv2.COLOR_GRAY2BGR))

smoothed_gaussian = VideoProccessor(eq_video,
    lambda im: cv2.GaussianBlur(im, (11, 11), 2))

mean_image = utils.mean_image(smoothed_gaussian, 0.001)
dif_video = VideoProccessor(smoothed_gaussian, 
    lambda im: transform.image_dif(im, mean_image))

thres = VideoProccessor(dif_video, 
    lambda im: cv2.adaptiveThreshold(im, 255,
        cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 151, -10))
morph = VideoProccessor(thres, transform.fill_gaps)
final = VideoProccessor([eq_video_color, morph], transform.filter_contours)
contoured = VideoProccessor([prep_video, final], transform.draw_contours)

# heatmap = VideoProccessor(morph, transform.heatmap_filter, 3)

# optical_flow = VideoProccessor(norm_video, transform.optical_flow, 2)
# color_flow = VideoProccessor(optical_flow, transform.color_flow)
# masked_flow = VideoProccessor([color_flow, final], utils.apply_mask)

# blended = VideoProccessor([eq_video_color, masked_flow], transform.draw_blend)

visualize.show_video_gallary([
    (source_video, 'source'),
    (norm_video, 'eq'),
    (dif_video, 'dif'),
    (morph, 'morph'),
    (contoured, 'contoured')
])

# mean_image = mean_image[110:110+500, 200:200+880]
# cv2.imshow('m', mean_image)
# cv2.waitKey(0)
# cv2.imwrite('data/util/mean.jpg', mean_image)