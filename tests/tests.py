import context as _
import cv2
import os
import glob
import numpy as np
import subprocess

src_path = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/Train/*'
dst_path = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/TrainPrep'

for f in glob.glob(src_path):
    d = f'{dst_path}/{os.path.basename(f)}' 

    subprocess.call(
        ['ffmpeg', '-y', '-i', f, '-vf', "fps=5,crop=880:500:200:110,scale=440:250", d]
            # stdout=subprocess.DEVNULL,
            # stderr=subprocess.DEVNULL
    )

