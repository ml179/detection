import tensorflow as tf
import cv2
import random
from keras.backend import set_session
import collections
import os
import keras
import numpy as np
from keras.layers import Conv2DTranspose, ConvLSTM2D, BatchNormalization, TimeDistributed, Conv2D, LayerNormalization
from keras.models import Sequential, load_model
import matplotlib.pyplot as plt
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip


class _Config:
    TRAIN = True
    ROOT = "/mnt/c/Users/binarycat/YandexDisk/FishMl"
    # DATASET_PATH = ROOT + '/normal_behavior'
    # SINGLE_TEST_PATH = "/content/drive/My Drive/Colab Notebook2/UCSD_Anomaly_Dataset.v1p2/UCSDped1/Test/Test032"
    BATCH_SIZE = 4
    EPOCHS = 3
    MODEL_PATH = "data/model_encoder/model.hdf5"
    STEPS_PER_EPOCH = 200
    NUM_FRAMES = 10
    NUM_SAMPLES = 20
    NUM_SELECTS = 2
    OUTPUT_SIZE = (256, 256)
    FRAME_STEP = 10
    CHANNEL_ORDER = [0]
    if TRAIN:
        CLASSES = ['day', 'night']
    else:
        CLASSES = ['object']
    # CLASSES = ['normal_behavior', 'loweringO2', 'increasingO2']  <-- старые классы


def format_frames(frame, output_size):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame = cv2.resize(frame, (256, 256))
    frame = frame.astype(np.float32)
    norm_frame = np.zeros((256, 256, 1))
    frame = cv2.normalize(frame, norm_frame, 0.0, 1.0, cv2.NORM_MINMAX)
    frame = frame.reshape((frame.shape[0], frame.shape[1], 1))
    frame = tf.image.convert_image_dtype(frame, tf.float32)
    frame = tf.image.resize_with_pad(frame, *output_size)
    return frame


def frames_from_video_file(video_path, n_frames, output_size, frame_step):
    # Read each video frame by frame
    result = []
    src = cv2.VideoCapture(str(video_path))

    video_length = src.get(cv2.CAP_PROP_FRAME_COUNT)

    need_length = 1 + (n_frames - 1) * frame_step

    if need_length > video_length:
        start = 0
    else:
        max_start = video_length - need_length
        start = random.randint(0, max_start + 1)

    src.set(cv2.CAP_PROP_POS_FRAMES, start)
    # ret is a boolean indicating whether read was successful, frame is the image itself
    ret, frame = src.read()
    result.append(format_frames(frame, output_size))

    for _ in range(n_frames - 1):
        for _ in range(frame_step):
            ret, frame = src.read()
        if ret:
            frame = format_frames(frame, output_size)
            result.append(frame)
        else:
            result.append(np.zeros_like(result[0]))
    src.release()
    result = np.array(result)[..., _Config.CHANNEL_ORDER]

    return result


# a = frames_from_video_file()


def get_files_and_class_names(subsets, num_samples=_Config.NUM_SAMPLES):
    video_paths = []
    classes = []
    for cls, files in subsets.items():
        files = files.copy()
        random.shuffle(files)
        for f in files[:num_samples]:
            video_paths.append(f)
            classes.append(cls)
    return video_paths, classes


class _FrameGenerator:
    def __init__(self, subsets, num_frames, num_selects=_Config.NUM_SELECTS, training=False):
        self.subsets = subsets
        self.num_frames = num_frames
        self.training = training
        self.num_selects = num_selects

        self.class_names = subsets.keys()
        self.class_ids_for_name = dict((name, idx) for idx, name in enumerate(self.class_names))

        self.video_paths, _ = get_files_and_class_names(self.subsets)
        if self.training:
            random.shuffle(self.video_paths)

        self.vid_index = 0
        self.pathes = []

    def __next__(self):
        if self.vid_index + self.num_selects >= len(self.video_paths):
            self.vid_index = 0
            random.shuffle(self.video_paths)
            # raise StopIteration()

        clips = []
        self.pathes = []

        for i in range(self.num_selects):
            path = self.video_paths[self.vid_index + i]
            self.pathes.append(path)
            video_frames = frames_from_video_file(path, self.num_frames)
            clips.append(video_frames)
        self.vid_index += self.num_selects

        clips = np.array(clips, ndmin=5)
        print(clips.shape)
        return clips, clips

    def last_pathes(self):
        return self.pathes

    def __iter__(self):
        return self


class _PrepareSubsets:

    def __init__(self):

        self.classes = _Config.CLASSES
        self.num_classes = len(self.classes)

    def get_files_per_class(self, root):
        files_for_class = collections.defaultdict(list)

        for class_dir in self.classes:
            for f in os.listdir(root + '/' + class_dir):
                full = root + '/' + class_dir + '/' + f

                # чтобы случайно не передали локальную директорию
                path = os.path.normpath(full)
                tokens = path.split(os.sep)
                if len(tokens) >= 2:
                    files_for_class[class_dir].append(full)

        return files_for_class

    def cut_video(files_for_class):
        timestamps = {"t0": 0, "t1": 15, "t2": 30, "t3": 45, "t4": 59}
        for class_name in files_for_class:
            for video in files_for_class[class_name]:
                for i in range(4):
                    print(f"Cutting video: {video}.mp4...")
                    ffmpeg_extract_subclip(f"{video}", timestamps["t" + str(i)], timestamps["t" + str(i + 1)],
                                           targetname=f"{video}_Cut_part#{i}.mp4")

    # def _select_subset_of_classes(files_for_class, classes, num_files_per_class):
    #     files_subset = dict()
    #
    #     for class_name in classes:
    #         if files_for_class[class_name].find("Cut_") != -1:
    #             class_files = files_for_class[class_name]
    #             files_subset[class_name] = class_files[:num_files_per_class]
    #
    #     return files_subset

    def _select_subset_of_classes(files_for_class, classes, num_files_per_class):
        files_subset = dict()
        for class_name in classes:
            vids = []
            for i in range(num_files_per_class + 1):
                video = files_for_class[class_name][i]
                if video.find("_Cut_") != -1:
                    vids.append(video)
                    
            files_subset[class_name] = vids

        return files_subset

    def _split_class_lists(self, files_for_class, count):
        split_files = {}
        remainder = {}
        for cls in files_for_class:
            split_files[cls] = files_for_class[cls][:count]
            remainder[cls] = files_for_class[cls][count:]
        return split_files, remainder

    def prepare_subsets(self, files_for_class, splits):
        for v in files_for_class.values():
            random.shuffle(v)

        # {название сплита: {имя класса: [список файлов]}}
        dirs = {}

        for split_name, split_count in splits.items():
            split_files, files_for_class = self._split_class_lists(files_for_class, split_count)
            # TODO скачивать с яндекс диска
            # download_from_zip(zip_url, split_dir, split_files)
            dirs[split_name] = split_files

        return dirs


# b = _PrepareSubsets()


class _CreateModel:

    def __init__(self):

        self.prep_subs = _PrepareSubsets()
        self.files_for_classes = self.prep_subs.get_files_per_class(root=_Config.ROOT)
        if _Config.TRAIN:
            splits = {"train": 20, "test": 0}
        else:
            splits = {"train": 0, "test": 20}

        self.subsets = self.prep_subs.prepare_subsets(files_for_class=self.files_for_classes, splits=splits)
        self.training_set = _FrameGenerator(self.subsets['train'], _Config.NUM_FRAMES, training=True)

    def _construct_model(self, reload_model=True):
        #      """
        # Parameters
        # ----------
        # reload_model : bool
        #     Load saved model or retrain it
        # """
        if not reload_model:
            return load_model(_Config.MODEL_PATH, custom_objects={'LayerNormalization': LayerNormalization})
            # output_signature = (
        #     tf.TensorSpec(shape = (None, 256, 256, 1), dtype = tf.float32), 
        #     tf.TensorSpec(shape = (None, 256, 256, 1), dtype = tf.float32)
        # )

        seq = Sequential()
        seq.add(TimeDistributed(Conv2D(128, (11, 11), strides=4, padding="same"),
                                batch_input_shape=(None, 10, 256, 256, 1)))
        seq.add(LayerNormalization())
        seq.add(TimeDistributed(Conv2D(64, (5, 5), strides=2, padding="same")))
        seq.add(LayerNormalization())
        # # # # #
        seq.add(ConvLSTM2D(64, (3, 3), padding="same", return_sequences=True))
        seq.add(LayerNormalization())
        seq.add(ConvLSTM2D(32, (3, 3), padding="same", return_sequences=True))
        seq.add(LayerNormalization())
        seq.add(ConvLSTM2D(64, (3, 3), padding="same", return_sequences=True))
        seq.add(LayerNormalization())
        # # # # #
        seq.add(TimeDistributed(Conv2DTranspose(64, (5, 5), strides=2, padding="same")))
        seq.add(LayerNormalization())
        seq.add(TimeDistributed(Conv2DTranspose(128, (11, 11), strides=4, padding="same")))
        seq.add(LayerNormalization())
        seq.add(TimeDistributed(Conv2D(1, (11, 11), activation="sigmoid", padding="same")))
        print(seq.summary())
        seq.compile(loss='mse', optimizer=keras.optimizers.Adam(learning_rate=1e-4, decay=1e-5, epsilon=1e-6))
        seq.fit(self.training_set,
                batch_size=_Config.BATCH_SIZE, epochs=_Config.EPOCHS, shuffle=False,
                steps_per_epoch=_Config.STEPS_PER_EPOCH)
        seq.save(_Config.MODEL_PATH)
        return seq


def evaluate():
    model = _CreateModel()
    print("got model")


class _TestModel(_CreateModel):

    def test_model(self):
        new_model = tf.keras.models.load_model(_Config.MODEL_PATH)
        if _Config.TRAIN:
            subset = self.subsets['train']
        else:
            subset = self.subsets['test']
        test_set = _FrameGenerator(subset, _Config.NUM_FRAMES, training=False)

        seq = []
        pathes = []
        for i in range(4):
            seq.append(test_set.__next__()[0])
            pathes.extend(test_set.last_pathes())

        seq = np.concatenate(seq, axis=0)
        sz = seq.shape[0]
        rec_seq = new_model.predict(seq, batch_size=4)

        sequences_reconstruction_cost = np.array([np.linalg.norm(np.subtract(seq[i], rec_seq[i])) for i in range(0, sz)])
        # sa = (sequences_reconstruction_cost - np.min(sequences_reconstruction_cost)) / np.max(sequences_reconstruction_cost)
        # sr = 1.0 - sa
        sr = sequences_reconstruction_cost

        plt.plot(sr)
        plt.ylabel('regularity score Sr(t)')
        plt.xlabel('frame t')
        plt.show()

# if _Config.TRAIN:
#     _Config.CLASSES = ['day', 'night']
# else:
#     _Config.CLASSES = ['object']
