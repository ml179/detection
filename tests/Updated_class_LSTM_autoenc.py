import time

import tensorflow as tf
import cv2
import random
import collections
import os
import keras
import numpy as np
from keras.layers import Conv2DTranspose, ConvLSTM2D, TimeDistributed, Conv2D, LayerNormalization
from keras.models import Sequential, load_model
import matplotlib.pyplot as plt
from moviepy.video.io.ffmpeg_tools import ffmpeg_extract_subclip
import shutil


class _Config:
    TRAIN = False
    ROOT = "."  # TODO не забудь поменять
    BATCH_SIZE = 4
    EPOCHS = 3
    MODEL_PATH = "model.hdf5"  # TODO не забудь поменять
    STEPS_PER_EPOCH = 200
    NUM_FRAMES = 10
    NUM_SAMPLES = 4
    NUM_SELECTS = 2
    OUTPUT_SIZE = (256, 256)
    FRAME_STEP = 10
    CHANNEL_ORDER = [0]
    if TRAIN:
        CLASSES = ['15s_videos']
    else:
        CLASSES = ['15s_videos']


def format_frames(frame, output_size):
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame = cv2.resize(frame, (256, 256))
    frame = frame.astype(np.float32)
    norm_frame = np.zeros((256, 256, 1))
    frame = cv2.normalize(frame, norm_frame, 0.0, 1.0, cv2.NORM_MINMAX)
    frame = frame.reshape((frame.shape[0], frame.shape[1], 1))
    frame = tf.image.convert_image_dtype(frame, tf.float32)
    frame = tf.image.resize_with_pad(frame, *output_size)
    return frame


def frames_from_video_file(video_path, n_frames, output_size, frame_step):
    # Read each video frame by frame
    result = []
    src = cv2.VideoCapture(str(video_path))

    video_length = src.get(cv2.CAP_PROP_FRAME_COUNT)

    need_length = 1 + (n_frames - 1) * frame_step

    if need_length > video_length:
        start = 0
    else:
        max_start = video_length - need_length
        start = random.randint(0, max_start + 1)

    src.set(cv2.CAP_PROP_POS_FRAMES, start)
    # ret is a boolean indicating whether read was successful, frame is the image itself
    ret, frame = src.read()
    result.append(format_frames(frame, output_size))

    for _ in range(n_frames - 1):
        for _ in range(frame_step):
            ret, frame = src.read()
        if ret:
            frame = format_frames(frame, output_size)
            result.append(frame)
        else:
            result.append(np.zeros_like(result[0]))
    src.release()
    result = np.array(result)[..., _Config.CHANNEL_ORDER]

    return result


def get_files_and_class_names(subsets, num_samples=_Config.NUM_SAMPLES):
    video_paths = []
    classes = []
    for cls, files in subsets.items():
        files = files.copy()
        #random.shuffle(files)
        for f in files[:num_samples]:
            video_paths.append(f)
            classes.append(cls)
    return video_paths, classes


class _FrameGenerator:
    def __init__(self, subsets, num_frames, num_selects=_Config.NUM_SELECTS, training=False):
        self.subsets = subsets
        self.num_frames = num_frames
        self.training = training
        self.num_selects = num_selects

        self.class_names = subsets.keys()
        self.class_ids_for_name = dict((name, idx) for idx, name in enumerate(self.class_names))

        self.video_paths, _ = get_files_and_class_names(self.subsets)
        if self.training:
            random.shuffle(self.video_paths)

        self.vid_index = 0
        self.pathes = []
    counter = 0

    def __next__(self):
        if self.vid_index + self.num_selects >= len(self.video_paths):
            self.vid_index = 0
            #random.shuffle(self.video_paths)
            # raise StopIteration()

        clips = []
        self.pathes = []

        #for i in range(self.num_selects):
        path = self.video_paths[self.vid_index + self.counter]
        print("path", path)
        self.pathes.append(path)
        video_frames = frames_from_video_file(path, self.num_frames, _Config.OUTPUT_SIZE, _Config.FRAME_STEP)
        clips.append(video_frames)
        self.vid_index += self.num_selects

        clips = np.array(clips, ndmin=5)
        print(clips.shape)
        self.counter += 1
        return clips, clips

    def last_pathes(self):
        return self.pathes

    def __iter__(self):
        return self


def cut_video(files_for_class):
    timestamps = {"t0": 0, "t1": 15, "t2": 30, "t3": 45, "t4": 59}
    for class_name in files_for_class:
        for video in files_for_class[class_name]:
            real_video_name = video[(video.find("day/")+4):]  # TODO Исправить мега-костыль с поиском имени файла
            print(real_video_name)
            for i in range(4):
                print(f"Cutting video: {video}.mp4...")
                ffmpeg_extract_subclip(f"{video}", timestamps["t" + str(i)], timestamps["t" + str(i + 1)],
                                       targetname=f"15s_videos/{real_video_name}_Cut_part#{i}.mp4")


class _PrepareSubsets:

    def __init__(self, path_to_vid):
        self.path_to_vid = path_to_vid
        self.classes = _Config.CLASSES
        self.num_classes = len(self.classes)

    def get_files_per_class(self, root):
        files_for_class = collections.defaultdict(list)

        # full = self.path_to_vid
        #
        # # чтобы случайно не передали локальную директорию
        # path = os.path.normpath(full)
        # tokens = path.split(os.sep)                      # РЕЗЕРВНАЯ КОПИЯ ДЛЯ ТЕСТОВ
        # if len(tokens) >= 2:
        #     files_for_class[self.classes[0]].append(full)

        for class_dir in self.classes:
            for f in os.listdir(root + '/' + class_dir):
                full = root + '/' + class_dir + '/' + f

                # чтобы случайно не передали локальную директорию
                path = os.path.normpath(full)
                tokens = path.split(os.sep)
                if len(tokens) >= 2:
                    files_for_class[class_dir].append(full)

        print(files_for_class)
        # files_for_class['default_class'].append(self.path_to_vid)
        # print("for_class", files_for_class)
        return files_for_class

    def _select_subset_of_classes(self, files_for_class, classes, num_files_per_class):
        files_subset = dict()
        for class_name in classes:
            vids = []
            for i in range(num_files_per_class + 1):
                video = files_for_class[class_name][i]
                if video.find("_Cut_") != -1:
                    print(video)
                    vids.append(video)

            files_subset[class_name] = vids

        return files_subset

    def _split_class_lists(self, files_for_class, count):
        split_files = {}
        remainder = {}
        for cls in files_for_class:
            split_files[cls] = files_for_class[cls][:count]
            remainder[cls] = files_for_class[cls][count:]
        return split_files, remainder

    def prepare_subsets(self, files_for_class, splits):
        for v in files_for_class.values():
            random.shuffle(v)

        # {название сплита: {имя класса: [список файлов]}}
        dirs = {}

        for split_name, split_count in splits.items():
            split_files, files_for_class = self._split_class_lists(files_for_class, split_count)
            dirs[split_name] = split_files

        return dirs


class _CreateModel:
    def __init__(self, path_to_vid="."):
        self.prep_subs = _PrepareSubsets(path_to_vid)
        self.files_for_classes = self.prep_subs.get_files_per_class(root=_Config.ROOT)

        if _Config.TRAIN:
            splits = {"train": 4, "test": 0}
        else:
            splits = {"train": 0, "test": 4}

        self.subsets = self.prep_subs.prepare_subsets(files_for_class=self.files_for_classes, splits=splits)
        self.training_set = _FrameGenerator(self.subsets['train'], _Config.NUM_FRAMES, training=True)


def split_videos_15s(path_to_vid):
    _Config.CLASSES = ['day']  # TODO Сюда нужно записать название папки, где будут появляться видосы, тк при делении
    # видоса на куски они парсятся через имена классов. См. метод get_files_per_class()
    # и желательно тоже сделай длиной в 3 символа, иначе оно крашится нафиг
    prep_subs = _PrepareSubsets(path_to_vid)
    files_for_classes = prep_subs.get_files_per_class(root=_Config.ROOT)
    cut_video(files_for_classes)
    _Config.CLASSES = ['15s_videos']  # TODO А сюда - имя папки где будут временно храниться 15-сек видосы


class Model_Control(_CreateModel):
    def prepare_model(self):
        shutil.rmtree("./15s_videos")
        os.mkdir("./15s_videos")

        self.new_model = tf.keras.models.load_model(_Config.MODEL_PATH)

        print("Model is now loaded!")

    def test_model(self, path_to_vid):
        split_videos_15s(path_to_vid)
        super().__init__(path_to_vid)

        if _Config.TRAIN:
            print("конфиг стоит на трейне")
            subset = self.subsets['train']
        else:
            print("конфиг на тесте")
            subset = self.subsets['test']

        test_set = _FrameGenerator(subset, _Config.NUM_FRAMES, training=False)

        seq = []
        pathes = []

        for i in range(_Config.BATCH_SIZE):  # здесь стояло range(4)
            seq.append(test_set.__next__()[0])
            pathes.extend(test_set.last_pathes())

        seq = np.concatenate(seq, axis=0)
        sz = seq.shape[0]
        rec_seq = self.new_model.predict(seq, batch_size=_Config.BATCH_SIZE)

        sequences_reconstruction_cost = np.array([np.linalg.norm(np.subtract(seq[i], rec_seq[i])) for i in range(0, sz)])

        sr = sequences_reconstruction_cost
        with open("predicts.txt", "a") as f:
            for i, s in enumerate(sr):
                f.write(f"{pathes[i]}: {s}\n")  # TODO Здесь сделай вывод куда надо предиктов (я для примера записываю в файл)


if __name__ == '__main__':

    model = Model_Control()
    model.prepare_model()  # Инициализация модели

    for i in range(99999):
        path_new = str(input("Введите путь к файлу"))
        model.test_model(path_new) # TODO СЮда вводим путь к файлу с видео
        time.sleep(1)                                       # Пример ввода пути: ./day/IPCAM_101-03-20230226011913.mp4
        shutil.rmtree("./15s_videos")  # Удаляем папку с обрезанными видео
        os.mkdir("./15s_videos")  # И создаем пустую заново
        print("Цикл завершен----------------------")

