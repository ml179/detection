import context as _
import detection.dbtools as dbtools

import tqdm
import random
import pathlib
import itertools
import collections
import wget
import tarfile

import os
import cv2
import numpy as np
#import remotezip as rz

import tensorflow as tf

# Some modules to display an animation using imageio.
import imageio
from urllib import request
#from tensorflow_docs.vis import embed

import seaborn as sns
import matplotlib.pyplot as plt

import keras
import tensorflow as tf
import tensorflow_hub as hub
from keras import layers
from keras.optimizers import Adam
from keras.losses import SparseCategoricalCrossentropy

# Import the MoViNet model from TensorFlow Models (tf-models-official) for the MoViNet model
from official.projects.movinet.modeling import movinet
from official.projects.movinet.modeling import movinet_model


def format_frames(frame, output_size):
        frame = tf.image.convert_image_dtype(frame, tf.float32)
        frame = tf.image.resize_with_pad(frame, *output_size)
        return frame    

def frames_from_video_file (video_path, n_frames, output_size, frame_step):
        # Read each video frame by frame
    result = []
    src = cv2.VideoCapture(str(video_path))  

    video_length = src.get(cv2.CAP_PROP_FRAME_COUNT)

    need_length = 1 + (n_frames - 1) * frame_step

    if need_length > video_length:
        start = 0
    else:
        max_start = video_length - need_length
        start = random.randint(0, max_start + 1)

    src.set(cv2.CAP_PROP_POS_FRAMES, start)
    # ret is a boolean indicating whether read was successful, frame is the image itself
    ret, frame = src.read()
    result.append(format_frames(frame, output_size))

    for _ in range(n_frames - 1):
        for _ in range(frame_step):
            ret, frame = src.read()
        if ret:
            frame = format_frames(frame, output_size)
            result.append(frame)
        else:
            result.append(np.zeros_like(result[0]))
    src.release()
    result = np.array(result)[..., _CONFIG.CHANNEL_ORDER]

    return result

def get_files_and_class_names(subsets):
    video_paths = []
    classes = []
    for cls, files in subsets.items():
        for f in files:
            video_paths.append(f)
            classes.append(cls)
    return video_paths, classes    

class _CONFIG():
    OUTPUT_SIZE = (224,224)
    FRAME_STEP = 15
    CHANNEL_ORDER = [2, 1, 0]
    CLASSES = ['normal_behavior', 'loweringO2', 'increasingO2']



class _FrameGenerator():
    def __init__(self, subset, num_frames, training = False):
        self.subsets = subset
        self.num_frames = num_frames
        self.training = training

        self.class_names = subset.keys()
        self.class_ids_for_name = dict((name, idx) for idx, name in enumerate(self.class_names))

    def __call__(self):
        video_paths, classes = get_files_and_class_names(self.subsets)

        pairs = list(zip(video_paths, classes))

        if self.training:
            random.shuffle(pairs)

        for path, name in pairs:
            video_frames = frames_from_video_file(path, self.num_frames, _CONFIG.OUTPUT_SIZE, _CONFIG.FRAME_STEP) 
            label = self.class_ids_for_name[name] # Encode labels
            yield video_frames, label 


class ModelTrainer(): 

    def __init__(self, num_frames, batch_size, model_id, resolution, num_epochs, subsets):
        self.num_frames = num_frames
        self.batch_size = batch_size
        self.model_id = model_id
        self.resolution = resolution
        self.num_epochs = num_epochs   
        self.subsets = subsets    

        self.output_signature = (tf.TensorSpec(shape = (None, None, None, 3), dtype = tf.float32),
                    tf.TensorSpec(shape = (), dtype = tf.int16))
        self.loss_obj = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
        self.optimizer = tf.keras.optimizers.Adam(learning_rate = 0.001)            

    def _prepearing_ds (self, subsets):

        fg_train = _FrameGenerator(subsets['train'], self.num_frames, training=True)
        frames, label = next(fg_train())
        fg_test = _FrameGenerator(subsets['test'], self.num_frames, training=True)

        train_ds = tf.data.Dataset.from_generator(fg_train, output_signature = self.output_signature)
        train_ds = train_ds.batch(self.batch_size)

        test_ds = tf.data.Dataset.from_generator( fg_test, output_signature = self.output_signature)
        test_ds = test_ds.batch(self.batch_size)

        return train_ds, test_ds

    def _loading_model (self):
        
        tf.keras.backend.clear_session()

        backbone = movinet.Movinet(model_id=self.model_id)
        backbone.trainable = False

        # Set num_classes=600 to load the pre-trained weights from the original model
        model = movinet_model.MovinetClassifier(backbone=backbone, num_classes=600)
        model.build([None, None, None, None, 3])

        url = 'https://storage.googleapis.com/tf_model_garden/vision/movinet/movinet_a0_base.tar.gz'
        filename = wget.download(url, 'data/model/')
        my_tar = tarfile.open(filename)
        my_tar.extractall('data/model') # specify which folder to extract to
        my_tar.close()

        # Load pre-trained weights
        # !wget https://storage.googleapis.com/tf_model_garden/vision/movinet/movinet_a0_base.tar.gz -O movinet_a0_base.tar.gz -q
        # !tar -xvf movinet_a0_base.tar.gz
        # !wget https://storage.googleapis.com/tf_model_garden/vision/movinet/movinet_a5_base.tar.gz -O movinet_a5_base.tar.gz -q
        # !tar -xvf movinet_a5_base.tar.gz

        checkpoint_dir = f'data/model/movinet_{self.model_id}_base'
        checkpoint_path = tf.train.latest_checkpoint(checkpoint_dir)
        checkpoint = tf.train.Checkpoint(model=model)
        status = checkpoint.restore(checkpoint_path)
        status.assert_existing_objects_matched()

        return model, backbone   

    def _build_classifier(self, batch_size, resolution, backbone, num_classes):
        """Builds a classifier on top of a backbone model."""
        model = movinet_model.MovinetClassifier(
            backbone=backbone,
            num_classes=num_classes)
        model.build([batch_size, 50, resolution, resolution, 3])

        return model   

    def _compile_model(self):
        backbone = self._loading_model()[1]
        num_classes = len(get_files_and_class_names(self.subsets)[1])
        model = self._build_classifier(self.batch_size, self.resolution, backbone, num_classes)
        model.compile(loss=self.loss_obj, optimizer=self.optimizer, metrics=['accuracy'])

        return model

    def train_and_evaluate (self):
        train_ds = self._prepearing_ds(self.subsets)[0]
        test_ds = self._prepearing_ds(self.subsets)[1]
        model = self._compile_model()
        results = model.fit(train_ds,
                    validation_data=test_ds,
                    epochs=self.num_epochs,
                    validation_freq=1,
                    verbose=1) 
        model.save('data/saved_models/my_model')                     

        return model, results, model.evaluate(test_ds, return_dict=True)              

class Model():
    def __init__(self, path_to_model, n_frames, model_id):

        self.path_to_model = path_to_model
        self.n_frames = n_frames
        self.model_id = model_id

        self.backbone = movinet.Movinet(model_id=self.model_id)
        self.backbone.trainable = False

        # Set num_classes=600 to load the pre-trained weights from the original model
        self.model = movinet_model.MovinetClassifier(backbone=self.backbone, num_classes=600)
        self.model.build([None, None, None, None, 3])

        self.new_model = tf.keras.models.load_model(self.path_to_model)

        self.output_signature = tf.TensorSpec(shape = (None, None, None, 3), dtype = tf.float32)


    def predict(self, path_to_video):     

        video_frames = frames_from_video_file(video_path=path_to_video, n_frames=self.n_frames, output_size=_CONFIG.OUTPUT_SIZE, frame_step=_CONFIG.FRAME_STEP)

        def gen():
            yield video_frames

        ds = tf.data.Dataset.from_generator(gen, output_signature = self.output_signature)
        ds = ds.batch(8)
        
        return self.new_model.predict(ds)

class _PrepareSubsets():

    def __init__(self):

        self.classes = _CONFIG.CLASSES
        self.num_classes = len(self.classes)
        

    def get_files_per_class(self, root):
        files_for_class = collections.defaultdict(list)

        for class_dir in self.classes:
            for f in os.listdir(root + '/' + class_dir):
                full = root + '/' + class_dir + '/' + f

                # чтобы случайно не передали локальную директорию
                path = os.path.normpath(full)
                tokens = path.split(os.sep)
                if len(tokens) >= 2:
                    files_for_class[class_dir].append(full)

        return files_for_class

    def _select_subset_of_classes(files_for_class, classes, num_files_per_class):
        files_subset = dict()

        for class_name in classes:
            class_files = files_for_class[class_name]
            files_subset[class_name] = class_files[:num_files_per_class]

        return files_subset    

    def _split_class_lists(self, files_for_class, count):
        split_files = {}
        remainder = {}
        for cls in files_for_class:
            split_files[cls] = files_for_class[cls][:count]
            remainder[cls] = files_for_class[cls][count:]
        return split_files, remainder    

    def prepare_subsets(self, files_for_class, splits):
        for v in files_for_class.values():
            random.shuffle(v)

        # {название сплита: {имя класса: [список файлов]}}
        dirs = {}

        for split_name, split_count in splits.items():
            split_files, files_for_class = self._split_class_lists(files_for_class, split_count)
            # TODO скачивать с яндекс диска
            #download_from_zip(zip_url, split_dir, split_files)
            dirs[split_name] = split_files

        return dirs    


prep_subs = _PrepareSubsets()
files_for_classes = prep_subs.get_files_per_class(root = DIR)
files_for_classes = dbtools.get_video_pathes()
subsets = prep_subs.prepare_subsets(files_for_class=files_for_classes, splits={"train": 80, "val": 20, "test": 20})      
#model_train = ModelTrainer(8, 8, 'a0', 172, 2, subsets)
#_, results, evaluation = model_train.train_and_evaluate()
saved_model = Model('data/saved_models/my_model', 8, 'a0')
#prediction = saved_model.predict('data/videos/day/IPCAM_101-05-20230209123015.mp4')
prediction = saved_model.predict('data/videos/test/day.mp4')

print()

class_names = list(files_for_classes.keys())
class_ids_for_name = dict((name, idx) for idx, name in enumerate(class_names))
print(class_names[np.argmax(prediction, axis=1)[0]])




