import context as _
import detection.preprocess as prep
import detection.utils as utils
import detection.filter as filter
import detection.transform as transform
import cv2 as cv
import numpy as np

def normalize(v):
    norm = np.linalg.norm(v)
    if norm == 0: 
       return v
    return v / norm

def prepare_frame(frame):
    frame = cv.resize(frame, (960, 540))
    frame = utils.crop_image(frame, 220, 0, 820, frame.shape[1])
    return frame, cv.cvtColor(frame, cv.COLOR_BGR2GRAY)


def draw_skelets(img, mask):
    img = img.copy()
    skelets = prep.find_skelets(mask)
    red = np.ones(skelets.shape)
    red = red*255
    img[:,:,2][skelets>0] = red[skelets>0]
    return img

path = 'data/videos/day.mp4'
capture = cv.VideoCapture(path)

if not capture.isOpened():
    exit(0)

masks = []

prev_frame = None
good_pred = None

#pathes = []
frame_cnt = 0

while True:
    ret, frame = capture.read()
    if frame is None:
        break

    frame, gray_frame = prepare_frame(frame)
    if prev_frame is None:
        prev_frame, prev_gray_frame = frame, gray_frame
    frame_cnt = frame_cnt + 1

    xray_frame = transform.xray(frame)
    morph_frame = transform.fill_gaps(xray_frame) 

    view_frame, contours_mask = transform.filter_contours(frame, morph_frame)
    #view_frame = draw_skelets(frame, contours_mask)

    #current_corners = cv.goodFeaturesToTrack(prev_gray_frame, 500, 0.001, 10, blockSize=30, mask=contours_mask)    
    #next_corners, status, error = cv.calcOpticalFlowPyrLK(prev_gray_frame, gray_frame, current_corners, None)

    #good_current = current_corners[status == 1]
    #good_next = next_corners[status == 1]

    # points = []
    # for prev, pred in zip(good_current, good_next):
    #     pred = pred.astype(np.uint).ravel()
    #     prev = prev.astype(np.uint).ravel()
    #     view_frame = cv.line(view_frame, prev, pred, (0, 0, 255), 2)
    #     dif = normalize(pred - prev)
    #     print(dif)
    #     points.append([pred[0], pred[1], dif[0], dif[1]])
        #good_pred.append((prev, pred))

    #points = np.array(points)
    #db = OPTICS(min_samples=3, max_eps=100).fit(points)

    #labels = db.labels_
    #print(labels)

    #n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    #unique_labels = set(labels)

    # colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(unique_labels))]
    # for k, col in zip(unique_labels, colors):
    #     if k == -1:
    #         col = [0, 0, 0, 1]

    #     class_member_mask = labels == k

    #     xy = points[class_member_mask]
    #     plt.plot(
    #         xy[:, 0],
    #         xy[:, 1],
    #         "o",
    #         markerfacecolor=tuple(col),
    #         markeredgecolor="k",
    #         markersize=14,
    #     )

    #     xy = points[class_member_mask]
    #     plt.plot(
    #         xy[:, 0],
    #         xy[:, 1],
    #         "o",
    #         markerfacecolor=tuple(col),
    #         markeredgecolor="k",
    #         markersize=6,
    #     )

    # plt.title(f"Estimated number of clusters: {n_clusters_}")
    #plt.show()

    if good_pred is not None:
        #lines = []

        for prev, pred in good_pred:
            min_dist = float('inf')

            # for current in good_current:
            #     current = current.astype(np.uint).ravel()
            #     dist = np.linalg.norm(current-pred)

            #     if dist < min_dist:
            #         min_dist = dist
            #         min_current = current

            if min_dist < 25:
                # lines.append((prev, min_current))
                view_frame = cv.line(view_frame, prev, pred, (0, 0, 255), 2)

        # for prev, current in lines:
        #     added = False
        #     for p in pathes:
        #         if p[-1][0][0] == prev[0] and p[-1][0][1] == prev[1] and p[-1][1] != frame_cnt:
        #             p.append((current, frame_cnt))
        #             added = True

        #     if not added:
        #         pathes.append([(prev, frame_cnt - 1), (current, frame_cnt)])
        
        # for path in pathes:
        #     if path[-1][1] == frame_cnt and len(path) > 2:
        #         for i in range(2):
        #             view_frame = cv.line(view_frame, path[i][0], path[i+1][0], (0, 0, 255), 2)
                     
    prev_frame, prev_gray_frame = frame, gray_frame 

    cv.imshow('frame', morph_frame)
    cv.moveWindow('frame', 0, 0)
    
    keyboard = cv.waitKey(30)
    if keyboard == 'q' or keyboard == 27:
        break

height, width, layers = masks[0].shape

#fourcc = cv.VideoWriter_fourcc(*'mp4v') 
#video = cv.VideoWriter('data/video/mask.avi', fourcc, 3, (width,height))

