import context as _
import detection.dbtools as db
import visual_for_front as visual
from fractions import Fraction
import subprocess
import datetime
import os
import glob
import gc

import cv2
import numpy
from av import VideoFrame

from aiortc import (
    RTCIceCandidate,
    RTCPeerConnection,
    RTCSessionDescription,
    VideoStreamTrack,
)
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder
from aiortc.contrib.signaling import BYE, add_signaling_arguments, create_signaling

size_thres = 1024 * 102
path = '/home/tatyana/Downloads/webcam1 (6).mp4'
record_to = 'detection/data/videos/test/out.mp4'
#vid_dir = "/home/binarycat/saved/*"
vid_dir = '/mnt/c/Users/binarycat/YandexDisk/FishMl/data/ped2/training/Train/*'


def lpath(p):
    return f'data/tmp/out.mp4'


def list_files(dir):
    res = []
    for f in sorted(glob.glob(dir), reverse=True):
        if os.path.getsize(f) <= size_thres:
            os.remove(f)
        else:
            res.append(f)
    return res


class DatabaseIterate:
    def current(self):
        return db.get_the_latest_video()

    def next(self):
        pass
       

class FolderIterate:
    def __init__(self, dir=vid_dir) -> None:
        self.dir = dir

    def current(self):
        res = list_files(self.dir)
        return res[1]

    def next(self):
        pass
       

class FolderDirIterate:
    def __init__(self,dir=vid_dir) -> None:
        self.files = list_files(dir)
        self.last_time = datetime.datetime.now()

        self.index = 0
    
    def current(self):
        cur_time = datetime.datetime.now()

        if (cur_time - self.last_time).seconds >= 60:
            self.last_time = cur_time
            self.index += 1
        res = self.files[self.index]
        return res
    
    def next(self):
        self.index += 1
        self.last_time = datetime.datetime.now()
    

class FlagVideoStreamTrack(VideoStreamTrack):
    def __init__(self):
        super().__init__()
        self.iter = FolderDirIterate()
        self.counter = 0 
        self.last_im = None
        
        self.last_time = None 
        self.last_path = None
        self.video = None

    async def recv(self):
        if self.last_time is None:
            self.last_time = datetime.datetime.now()
        cur_time = datetime.datetime.now()
        timer = (cur_time - self.last_time).seconds

        new_path = self.iter.current()
        if new_path is None:
            new_path = 'data/videos/test/asd.mp4'

        if new_path != self.last_path:
            self.last_path = new_path
            self.last_time = cur_time
            to_path = lpath(self.last_path)

            subprocess.call(
                ['ffmpeg', '-y', '-i', self.last_path, '-vf', "fps=5,crop=880:500:200:110", to_path],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL
            )
            print('video copied')
            if self.video is not None:
                print('release')
                self.video.release()
            self.video = visual.vizualization(to_path)

        time_base = Fraction(1, 10240)
        pts = int(timer * 5) * 2048
        idx = int(timer * 5)

        # при завершении фрагмента видео
        if idx >= 290:
            idx = 286
            self.iter.next()
        
        try:
            self.last_im = self.video[idx]
        except:
            self.iter.next()

        new_frame = VideoFrame.from_ndarray(self.last_im, format="bgr24")
        new_frame.pts = pts
        new_frame.time_base = time_base

        gc.collect()

        return new_frame    


# async def run(pc, recorder, signaling, role):
#     def add_tracks():
#         print('adslsdffsadff')
#         video = vis.vizualization(path)
#         pc.addTrack(FlagVideoStreamTrack(video))

#     @pc.on("track")
#     def on_track(track):
#         print("Receiving %s" % track.kind)
#         recorder.addTrack(track)

#     # connect signaling
#     await signaling.connect()

#     if role == "offer":
#         # send offer
#         add_tracks()
#         await pc.setLocalDescription(await pc.createOffer())
#         await signaling.send(pc.localDescription)

#     # consume signaling
#     while True:
#         obj = await signaling.receive()

#         if isinstance(obj, RTCSessionDescription):
#             await pc.setRemoteDescription(obj)
#             await recorder.start()

#             if obj.type == "offer":
#                 # send answer
#                 add_tracks()
#                 await pc.setLocalDescription(await pc.createAnswer())
#                 await signaling.send(pc.localDescription)
#         elif isinstance(obj, RTCIceCandidate):
#             await pc.addIceCandidate(obj)
#         elif obj is BYE:
#             print("Exiting")
#             break


# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(description="Video stream from the command line")
#     parser.add_argument("--role", choices=["offer", "answer"], default='offer')
#     parser.add_argument("--verbose", "-v", action="count")
#     add_signaling_arguments(parser)
#     args = parser.parse_args()

#     if args.verbose:
#         logging.basicConfig(level=logging.DEBUG)

#     # create signaling and peer connection
#     signaling = create_signaling(args)
#     pc = RTCPeerConnection()

#     # create media sink
#     if record_to:
#         recorder = MediaRecorder(record_to)
#     else:
#         recorder = MediaBlackhole()

#     # run event loop
#     loop = asyncio.get_event_loop()
#     try:
#         loop.run_until_complete(
#             run(
#                 pc=pc,
#                 recorder=recorder,
#                 signaling=signaling,
#                 role=args.role,
#             )
#         )
#     except KeyboardInterrupt:
#         pass
#     finally:
#         # cleanup
#         loop.run_until_complete(recorder.stop())
#         loop.run_until_complete(signaling.close())
#         loop.run_until_complete(pc.close())
#