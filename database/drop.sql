DROP TABLE IF EXISTS pred_labels CASCADE;
DROP TABLE IF EXISTS model_tests CASCADE;
DROP TABLE IF EXISTS models CASCADE;
DROP TABLE IF EXISTS video_tags CASCADE;
DROP TABLE IF EXISTS tags CASCADE;
DROP TABLE IF EXISTS video_labels CASCADE;
DROP TABLE IF EXISTS videos CASCADE;

/*
DROP TABLE IF EXISTS labels CASCADE;
DROP TABLE IF EXISTS cameras CASCADE;
*/